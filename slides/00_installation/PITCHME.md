---?image=assets/img/grass.png&position=bottom&size=100% 30%

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Instalemos @color[green](GRASS GIS)

<br>

---

## @fa[windows] MS Windows @fa[windows]
<br>
@fa[download text-green] Descargar el instalador de **OSGeo4W** @fa[download text-green]
https://trac.osgeo.org/osgeo4w
<br><br>

> *@size[28px](Importante:)*
> @size[28px](Click derecho sobre el instalador y ejecutar con permiso de **Administrador**)

+++

@snap[south-west list-content-concise span-100]
@ol[](false)
- Seleccionar **Advance install**
- Seleccionar **Install from internet**
- Dejar el *directorio de instalación* configurado por defecto
- Elegir el servidor de *osgeo* para descargar el software
- En la sección *Desktop applications*, seleccionar **GRASS GIS stable** y **QGIS desktop**
- En la sección *Lib*, seleccionar **qgis-grass-plugin7**, **matplotlib**, **python-pip**, **python-ply** y **python-pandas** 
- En la sección *Command line utilities* seleccionar **msys**
- Esperar que finalice la descarga e instalación, y listo!
@olend
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](1.)
@snapend

@snap[east span-70]
Seleccionar **Advance install**
<br>
<img src="assets/img/osgeo4w_step_1.png">
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](2.)
@snapend

@snap[east span-70]
Seleccionar **Install from internet**
<br>
<img src="assets/img/osgeo4w_step_2.png">
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](3.)
@snapend

@snap[east span-70]
Dejar el "Install directory" que viene por defecto
<br>
<img src="assets/img/osgeo4w_step_3.png">
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](4.)
@snapend

@snap[east span-70]
Elegir el servidor de *osgeo*
<br>
<img src="assets/img/osgeo4w_step_4.png">
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](5.)
@snapend

@snap[east span-70]
En la sección *Desktop applications*, seleccionar **GRASS GIS** y **QGIS desktop**
<br>
<img src="assets/img/osgeo4w_step_5.png">
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](6.)
@snapend

@snap[east span-70]
En la sección *Lib*, seleccionar **qgis-grass-plugin7**, **matplotlib**, **python-scipy**, **python-pip**, **python-ply** y **python-pandas** 
<br>
<img src="assets/img/osgeo4w_step_6.png">
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](7.)
@snapend

@snap[east span-70]
En la sección, *Command line utilities* seleccionar **msys**
<br>
<img src="assets/img/osgeo4w_step_7.png">
@snapend

@snap[south-east span-80]
@size[20px](**Nota**: el instalador resolverá y descargará las dependencias necesarias)
<br><br>
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](8.)
@snapend

@snap[east span-70]
Esperar que finalice la descarga e instalación, y listo! @fa[grin-alt text-orange]
<br>

![last step](assets/img/osgeo4w_step_10.png)
@snapend

+++?color=linear-gradient(90deg, #8EA33B 30%, white 30%)

@snap[west text-white]
@size[3em](9.)
@snapend

@snap[east span-70]
Para actualizar, sólo se debe abrir "OSGeo4W Setup" y seguir los pasos descritos anteriormente
<br><br>
**@size[64px](Super facil!)**
@fa[rocket text-orange fa-2x]
@snapend

---

## @fa[linux] Linux @fa[linux]
<br>
- **Ubuntu**: Instalar GRASS GIS desde el repositorio *unstable*


```bash
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
sudo apt-get install grass grass-gui grass-dev
```

+++

## @fa[linux] Linux @fa[linux]
<br>
- **Fedora**: Instalar GRASS GIS desde los repositorios oficiales


```bash
sudo dnf install grass grass-gui grass-devel
```
<br>
@size[26px](Otras distros: https://grass.osgeo.org/download/)

---

### Otros paquetes necesarios

@ul
- **[pymodis](http://www.pymodis.org/)**: librería para trabajar con datos MODIS. Ofrece descarga, mosaico, re-proyección, conversión de formato y extracción de bandas de calidad. Es necesaria para *[i.modis](https://grass.osgeo.org/grass7/manuals/addons/i.modis.html)*.
- **[landsatxplore](https://github.com/yannforget/landsatxplore)**: utilidad para búsqueda y descarga de imágenes Landsat 5, 7 y 8 desde [Earth Explorer](http://earthexplorer.usgs.gov/). Es necesaria para *[i.landsat](https://grass.osgeo.org/grass7/manuals/addons/i.landsat.html)*.
@ulend

+++

### Otros paquetes necesarios (cont.)

@ul
- **[sentinelsat](https://github.com/sentinelsat/sentinelsat)**: utilidad para búsqueda y descarga de imágenes Copernicus Sentinel del [Copernicus Open Access Hub](https://scihub.copernicus.eu/). Es necesaria para *[i.sentinel](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.html)*.
- **[scikit-learn](https://scikit-learn.org/stable/)**: librería de python para machine learning. Es requerida por la extensión *[r.learn.ml](https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html)*.
@ulend

+++

### Instalar pymodis, landsatxplore, sentinelsat y scikit-learn en Windows @fa[windows]
<br>
- Abrir la **OSGeo4W shell** y ejecutar:


```python
pip install setuptools
pip install pymodis
pip install landsatxplore
pip install sentinelsat
pip install scikit-learn
```

+++

### Instalar pymodis, landsatxplore, sentinelsat y scikit-learn en Mac @fa[apple] y Linux @fa[linux]
<br>
- Abrir una terminal y ejecutar:


```python
pip install setuptools
pip install pymodis
pip install landsatxplore
pip install sentinelsat
pip install pandas
pip install scikit-learn
```

---

@snap[north span-100]
### Datos para el curso
@snapend

@snap[west span-100 text-09]
@ul[](false)
- [**Location North Carolina (150Mb)**](https://grass.osgeo.org/sampledata/north_carolina/nc_spm_08_grass7.zip): descomprimir en *`$HOME/grassdata`*
- [**Location Córdoba (150Mb)**](https://drive.google.com/file/d/1uTwTy34vtqqxW_8sjWKF4zSpcXHkBFko/view?usp=sharing): descomprimir en *`$HOME/grassdata`*
- [Mapas raster (1Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/3b11ad06d2133889e0ee51652a03f94bfec9d7e4/data/sample_rasters.zip?inline=false): descomprimir en *`$HOME/gisdata`*
- [Mapa vectorial (3Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/streets.gpkg?inline=false): mover a *`$HOME/gisdata`*
- [Datos SPOT para OBIA](https://aulavirtual.ig.conae.unc.edu.ar/): Estos datos estarán en el aula virtual por tener licencia especial.
@ulend
@snapend

+++

@snap[north span-100]
### Datos para el curso
@snapend

@snap[west span-100 text-09]
Presentación y ejercicios opcionales (pero recomendados):
<br><br>
@ul[](false)
- Escenas [Landsat 8 14/01/2020 (979Mb)](https://drive.google.com/file/d/1ytQp-xin1FQr_hqtDJRLgK6g4eXwK-WI/view?usp=sharing) y [Landsat 8 02/03/2020 (880Mb)](https://drive.google.com/file/d/1Gg8FbhwpIQR-GyYepM4uw_9IOjEnji_N/view?usp=sharing): mover a *`$HOME/gisdata/landsat_data`*, NO DESCOMPRIMIR.
- Escena [Sentinel 2 (720Mb)](https://drive.google.com/file/d/1RbTMjvElQX_EOwE07GXYOq4rJohCOTvq/view?usp=sharing): mover a *`$HOME/gisdata/s2_data`*, NO DESCOMPRIMIR.
@ulend
@snapend

+++

El árbol de directorios en *`$HOME/grassdata`* debería quedar así:

```bash
/home/username/grassdata/
├── nc_spm_08_grass7
│   ├── landsat
│   ├── PERMANENT
│   └── user1
└── posgar2007_4_cba
    ├── modis_lst
    ├── modis_ndvi
    └── PERMANENT
```

> @size[24px](@fa[exclamation-triangle text-orange] **Nota:** `/home/username/` puede ser `User/Documents` en Windows, por ejemplo)

+++

Los archivos dentro del directorio `$HOME/gisdata` debieran ser:

- Mapas raster
- Mapa vectorial
- Escenas de L8 dentro de la carpeta `landsat_data` (*NO DESCOMPRIMIR*)
- Escena de S2 dentro de la carpeta `s2_data` (*NO DESCOMPRIMIR*)

---

### Una cosa más...
<br>
Registrarse en:

- USGS: https://ers.cr.usgs.gov/register

- NASA services: https://urs.earthdata.nasa.gov/users/new

- ESA-Copernicus services: https://scihub.copernicus.eu/dhus/#/self-registration

+++

Crear 3 archivos, 

*`USGS_SETTING.txt`*, *`NASA_SETTING.txt`* y *`SENTINEL_SETTING.txt`*, 

cada uno con el siguiente contenido:

```
your_username
your_password
```

y guardarlos en *`$HOME/gisdata`*

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## **Estamos listos! Comencemos!**

---

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
