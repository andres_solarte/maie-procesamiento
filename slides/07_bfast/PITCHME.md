---?image=assets/img/grass.png&position=bottom&size=100% 30%

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Análisis de series de tiempo y detección de cambios con BFAST

<br>

+++

@snap[north-west span-60]
<h3>Contenidos</h3>
@snapend

@snap[west span-100]
<br>
@ol[list-content-verbose]
- Nociones básicas
- BFAST: Breaks For Additive Seasonal and Trend
- Manos a la obra: BFAST temporal
- Manos a la obra: BFAST espacio-temporal
@olend
@snapend

---

### Nociones básicas

Detectar y caracterizar los @css[text-green](cambios ambientales en el tiempo) es el primer
paso para identificar el motor del cambio y comprender su mecanismo

Los @fa[satellite fa-2x text-gray] son adecuados para esta tarea porque proporcionan 
mediciones consistentes y repetidas a una **escala espacial apropiada 
para captar los efectos de cambios naturales y antrópicos**

+++

### Cambio en los ecosistemas

@ol[list-content-verbose](false)
- **Cambio estacional**, impulsado por las interacciones anuales de la temperatura y la lluvia que afectan a la fenología de las plantas 
- **Cambio gradual**, como la variabilidad climática interanual o el cambio gradual de la ordenación o la degradación de las tierras
- **Cambio abrupto**, causado por perturbaciones como la deforestación, la urbanización, las inundaciones y los incendios
@olend

@img[span-45](https://berkeleyearth.org/wp-content/uploads/2020/01/2019_Time_Series-768x433.png)
@img[span-45](https://i2.wp.com/www.intelligentliving.co/wp-content/uploads/2020/05/13th-3.jpg?resize=960%2C540&ssl=1)

+++

Se han desarrollado una serie de métodos de detección de cambios en series de tiempo

@fa[arrow-down fa-3x text-green]

2 limitantes principales

+++?color=linear-gradient(90deg, #8EA33B 25%, white 25%)

@snap[west span-20 text-white]
@size[92px](1)
@snapend

@snap[east span-80 text-left]
Dificultad en la detección/estimación del cambio en datos de teledetección:
@ul[](false) 
- Cambios estacionales, 
- Cambios graduales,
- Cambios abruptos, 
- Ruido por errores geométricos remanentes, dispersión atmosférica y efectos de las nubes
@snapend

+++?color=linear-gradient(90deg, #8EA33B 25%, white 25%)

@snap[west span-20 text-white]
@size[92px](1)
@snapend

@snap[east span-80 text-left]
@ul[](false)
- La mayoría de los métodos se centran en series temporales cortas (2-5 img)
- Varios enfoques: *Análisis de Componentes Principales* (PCA), el análisis de *Fourier* y el *Análisis de Vectores de Cambio* (CVA)
- Discriminan el ruido de la señal por su características temporales e implican algún tipo de transformación
@snapend

+++?color=linear-gradient(90deg, #8EA33B 25%, white 25%)

@snap[west span-20 text-white]
@size[92px](1)
@snapend

@snap[east span-80 text-left]
@ul[](false)
- El CVA sólo puede realizarse entre dos períodos de tiempo
- El análisis depende fuertemente de la selección de esos períodos
- Se minimiza la variación estacional centrándose en períodos específicos dentro de un año, resumiendo temporalmente los datos o normalizando los valores de reflectancia por tipo de cobertura
@ulend
@snapend

+++?color=linear-gradient(90deg, #8EA33B 25%, white 25%)

@snap[west span-20 text-white]
@size[92px](2)
@snapend

@snap[east span-80 text-left]
@ul[](false)
- Deben ser independientes de umbrales o de trayectorias de cambio
@ulend
@ul[text-08](false)
  - Métodos que requieren umbrales @fa[arrow-right] resultados engañosos debido a diferencias espectrales y fenológicas @fa[arrow-right] difíciles de llevar a escalas globales 
  - Métodos basados en trayectorias caracterizan el cambio por su firma temporal @fa[arrow-right] sólo funcionará si la trayectoria espectral observada coincide con una de las hipotéticas
@ulend
@snapend

---

### BFAST: Breaks For Additive Seasonal and Trend

@img[span-65](assets/img/bfast_paper.png)

@size[20px](Verbesselt et al. 2010. Detecting trend and seasonal changes in satellite image time series. <a href="https://doi.org/10.1016/j.rse.2009.08.014">DOI</a>)

+++

### BFAST: Breaks For Additive Seasonal and Trend

- Enfoque genérico de detección de cambios para series temporales
- Integra descomposición iterativa de series temporales en tendencia, variación estacional y ruido con métodos de detección de puntos de cambio (breakpoints)
- No es necesario seleccionar período de referencia, fijar un umbral o definir una trayectoria de cambio
- Puede automatizarse y escalarse

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Manos a la obra

+++

@fa[download text-green fa-2x] Descargar el [código](https://gitlab.com/veroandreo/maie-procesamiento/raw/master/code/07_bfast.r?inline=false) 
para seguir esta sesión e iniciar GRASS GIS en el mapset *`modis_ndvi`*
<br>

```bash zoom-13
grass78 grassdata/posgar2007_4_cba/modis_ndvi --text
```

+++

Exportar la serie de tiempo de NDVI desde GRASS GIS para importarla luego en R
<br><br>

```bash zoom-15
t.list 

t.rast.export input=ndvi_monthly_patch \
  output=ndvi_monthly.tar.gzip \
  directory=$HOME \
  compression=gzip
```
<br>

@fa[exclamation-triangle text-orange] @size[24px](Se podrían usar directamente los archivos `.hdf` originales también. Ver <a href="http://www.loicdutrieux.net/bfastSpatial/">tutorial</a>.)

+++

Desde la terminal de GRASS, iniciamos @fab[r-project fa-3x text-blue]

```
rstudio &
```

+++

@fa[exclamation-triangle text-orange] Atención usuarios Windows 10 @fa[exclamation-triangle text-orange] 

<br>

- Actualizar RStudio a la última versión (1.3.1073)
- Instalar [R-4.0.2](https://cran.r-project.org/bin/windows/base/)
- Instalar [Rtools 4.0](https://cran.r-project.org/bin/windows/Rtools/)
- Reiniciar RStudio si quedó abierto

<br>
<p class="text-07">En el caso de obtener el siguiente error <br>
`During startup - Warning message: Setting LC_CTYPE= failed` <br>
en RStudio, seguir los pasos descriptos [aquí](https://stackoverflow.com/questions/54102869/during-startup-warning-message-setting-lc-ctype-failed-on-windows)</p>

+++

### Importar y preparar datos

@code[r zoom-13](code/07_bfast.r)

@[8-18](Cargar las librerías que vamos a utilizar)
@[20-22](Importar el vector con los límites provinciales - sólo para referencia)
@[24-26](Leer la serie de tiempo)
@[28-30](Obtener información sobre el stack)
@[32-33](Visualizar una de las capas con *mapview*)
@[35-36](Convertir a *brick*)
@[38-39](Asignar las fechas al brick para que sea una serie de tiempo)
@[41-42](Corroborar la fecha de alguna capa dentro del brick)
@[44-46](Obtener información del brick)
@[48-49](Visualizar una de las capas con *mapview*)
@[51-52](Animación)

---

### BFAST temporal

@code[r zoom-13](code/07_bfast.r)

@[54-55](Definir un número de pixel)
@[57-61](Crear la serie de tiempo para el pixel seleccionado)
@[63-65](Graficar)

+++

@size[26px](Pixel 1269)

@img[span-75](assets/img/ndvi_ts.png)

> @fa[tasks] Graficar la serie de tiempo para otros pixeles

+++

### BFAST temporal

@code[r zoom-13](code/07_bfast.r)

@[70-75](Ejecutar BFAST para un pixel)
@[77-79](Graficar los resultados)

+++

@img[span-75](assets/img/bfast_single_pixel.png)

+++

> @fa[tasks] **Tarea**
> 
> - Ver el manual de la función `bfast()` y probar con otras configuraciones y otros pixeles
> - Graficar los resultados

+++

BFASTmonitor sobre un pixel

@code[r zoom-13](code/07_bfast.r)

@[81-85](Ejecutar *BFASTmonitor* sobre un pixel)
@[87-88](Inspeccionar el objeto resultante)

+++

> @fa[tasks] **Tarea**
>
> - Qué diferencias hay entre `bfast()` y `bfmPixel()`?
> - Ejecutar `bfmPixel()` para con distintas configuraciones y para distintos pixeles
> - Cambia la ubicación de los breakpoints?

---

### BFAST espacio-temporal

@code[r zoom-13](code/07_bfast.r)

@[109-112](Ejecutar *BFASTmonitor* sobre el *brick*)
@[114-115](Inspeccionar el objeto resultante)
@[117-118](Graficar)

+++

@img[span-100](assets/img/bfm_spatial.png)

+++

### Cuándo ocurren los cambios?

@code[r zoom-13](code/07_bfast.r)

@[120-129](Extraer el raster de **cambio** y convertir los decimales a mes)
@[131-138](Graficar con *tmap*)

+++

@img[span-90](assets/img/bfm_spatial_change.png)

+++

### Magnitud de los cambios

@code[r zoom-13](code/07_bfast.r)

@[140-141](Extraer y re-escalar el raster de **magnitud**)
@[143-145](Quedarse sólo con los pixeles donde se detectó un **breakpoint**)
@[147-148](Hacer stack)
@[150-158](Graficar)

+++

@img[span-80](assets/img/bfm_spatial_magn_bkp.png)

+++

### Magnitud de los cambios

@code[r zoom-13](code/07_bfast.r)

@[160-162](Aplicar umbral de cambio)
@[164-165](Hacer stack)
@[167-175](Graficar)

+++

@img[span-80](assets/img/bfm_spatial_thres.png)

+++

### Magnitud de los cambios

@code[r zoom-13](code/07_bfast.r)

@[177-179](Remover áreas menores a 3 pixeles)
@[181-182](Hacer stack)
@[184-193](Graficar)

+++

@img[span-80](assets/img/bfm_spatial_rmarea.png)

+++

### Magnitud de los cambios

@code[r zoom-13](code/07_bfast.r)

@[195-197](Agrupar parches contiguos)
@[199-200](Hacer stack)
@[202-210](Graficar)

+++

@img[span-80](assets/img/bfm_spatial_sizes.png)

+++

### Magnitud de los cambios

@code[r zoom-13](code/07_bfast.r)

@[212-213](Estimar tamaño de parches en has)
@[215-222](Graficar)
@[224-226](Imprimir estadísticas de los parches)
@[228-230](Exportar mapa a GRASS)

+++

@img[span-50](assets/img/bfm_spatial_has.png)

+++

> @fa[tasks] Analizar los resultados:
>
> - Es apropiada la resolución espacial de los datos utilizados?
> - Es apropiada la longitud de la serie de tiempo?
> - Qué fenómenos podrían haber causado los cambios detectados? 
> - Cómo lo comprobarían? 

---

## @fa[bookmark text-green] Recursos interesantes @fa[bookmark text-green] 

- [Paquete bfastSpatial](https://github.com/loicdtx/bfastSpatial)
- [Tutorial bfastSpatial](http://www.loicdutrieux.net/bfastSpatial/)
- [MODIS based time series analysis using BFASTmonitor](https://verbe039.github.io/BFASTforAEO/)
- [Preparar datos irregulares para BFAST](https://philippgaertner.github.io/2018/04/bfast-preparation/#)
- [dtwSat: Dynamic Time Warping for Satellite Time Series Analysis in R](https://github.com/vwmaus/dtwSat/)

---

## @fa[book text-green] Referencias @fa[book text-green]

@ol[list-content-verbose](false)
- Verbesselt, J., Hyndman, R., Newnham, G., & Culvenor, D. (2010). Detecting trend and seasonal changes in satellite image time series. [DOI](https://doi.org/10.1016/j.rse.2009.08.014)
- Verbesselt, J., Hyndman, R., Zeileis, A., & Culvenor, D. (2010). Phenological change detection while accounting for abrupt and gradual trends in satellite image time series. [DOI](https://doi.org/10.1016/j.rse.2010.08.003)
- Verbesselt, J., Zeileis, A., & Herold, M. (2012). Near real-time disturbance detection using satellite image time series. [DOI](https://doi.org/10.1016/j.rse.2012.02.022)
@olend

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

## THE END! 

@fa[grin-beam-sweat fa-3x text-green]

<br><br>
@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
