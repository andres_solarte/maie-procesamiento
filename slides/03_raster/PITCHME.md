---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos raster en GRASS GIS

<br>

---

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br>
@ol[list-content-verbose]
- Nociones básicas sobre datos raster en GRASS GIS
- Valores NULL (no-data)
- Máscaras
- Región computacional y datos raster
- Reportes y estadísticas
- Regresión
- Álgebra de mapas
@olend
@snapend

---

### Datos raster en GRASS GIS

> Un *mapa raster* es un arreglo de celdas en forma de grilla. Tiene filas y columnas y en cada celda hay datos o un indicador de no-data. Pueden ser arreglos 2D o 3D.

- Los límites se describen en los campos norte (n), sur (s), este (e) y oeste (w). 
- La extensión se calcula a partir de los límites externos de todas las celdas del mapa.

@size[20px](Para más info ver la página: <a href="https://grass.osgeo.org/grass-stable/manuals/rasterintro.html">Raster Intro</a>)

+++

### Precisión de datos raster

- **CELL DATA TYPE:** un mapa raster de tipo ENTERO (sólo números enteros)
- **FCELL DATA TYPE:** un mapa raster de tipo FLOTANTE (4 bytes, 7-9 dígitos de precisión)
- **DCELL DATA TYPE:** un mapa raster de tipo DOBLE (8 bytes, 15-17 dígitos de precisión)

@size[22px](Para más info ver la wiki: <a href="https://grasswiki.osgeo.org/wiki/GRASS_raster_semantics">Raster semantics</a>)

+++

### Reglas generales para raster en GRASS

- Mapas raster de @color[#8EA33B](**salida u output**) tienen sus *límites y resolución iguales a los de la región computacional*
- Mapas raster de @color[#8EA33B](**entrada o input**) son automáticamente *cortados y reajustados a la región computacional*
- Mapas raster de @color[#8EA33B](**entrada o input**) se enmascaran automáticamente si existe un mapa raster llamado *MASK*.

+++

@fa[exclamation-triangle text-orange fa-3x]

> **Excepción:** Todos los módulos @color[#8EA33B](r.in.*) leen los datos celda por celda sin remuestreo (a menos que se especifique lo contrario)

---

### NULL: valores nulos en GRASS GIS

- **NULL** representa "sin dato" en los mapas raster, lo que difiere de 0 (cero)
- Operaciones con celdas NULL producen celdas NULL
- Los valores NULL son gestionados con [r.null](https://grass.osgeo.org/grass-stable/manuals/r.null.html)

```bash
# establecer el valor no-data
r.null map=mapname setnull=-9999

# reemplazar NULL por un numero 
r.null map=mapname null=256
```

---

### @fa[mask text-green] Máscaras en GRASS GIS @fa[mask text-green]

- Se puede crear un mapa raster llamado *MASK* para enmascarar ciertas áreas
- Todas las celdas que sean NULL en el mapa MASK serán ignoradas (también todas las áreas fuera de la región computacional).
- Las máscaras se establecen con [r.mask](https://grass.osgeo.org/grass-stable/manuals/r.mask.html) o creando un raster con el nombre *MASK*. 

+++

Los mapas vectoriales también pueden usarse como máscaras

![MASK](assets/img/masks.png)
<br>
@size[22px](a- Raster *elevation* y vector *lakes*. b- Sólo los datos raster dentro de la máscara son usados para análisis posteriores. c- Máscara inversa.)

+++

Ejemplos de máscaras

```bash
# usar un vector como máscara
r.mask vector=lakes

# usar un vector como máscara inversa
r.mask -i vector=lakes

# enmascarar solo algunos valores de un mapa raster 
r.mask raster=landclass96 maskcats="5 thru 7"

# crear un raster MASK
r.mapcalc expression="MASK = if(elevation < 100, 1, null())"

# remover la máscara
r.mask -r
```

@size[22px](**Nota**: Una máscara sólo se aplica realmente cuando se lee un mapa raster, es decir, cuando se usa como entrada en un módulo.)

---

### Región computacional

![Show computational region](assets/img/region.png)

@size[26px](Puede definirse y modificarse mediante <a href="https://grass.osgeo.org/grass-stable/manuals/g.region.html">g.region</a> a la extensión de un mapa vectorial, un raster o manualmente a alguna zona de interés.)

+++

- La @color[#8EA33B](**región computacional**) está definida en función de la extensión dada por los límites norte, sur, este y oeste y una resolución espacial. *Aplica únicamente a las operaciones con datos raster.* 
- La @color[#8EA33B](**región de un mapa raster**) está definida por la extensión del mapa y la resolución del mapa. Cada mapa raster tiene su región, pero la región computacional tiene precedencia. 
- La @color[#8EA33B](**región de visualizacion**) es la extensión del *map display* independiente de la región computacional y la región del mapa raster. 

+++

Es posible establecer la región computacional a partir de la región de visualización

@img[width=600px](assets/img/save_region_from_display.png)

---

### Importar/exportar, máscara y región

- @color[#8EA33B](r.in.\* & r.import) importan siempre el mapa completo (a menos que se establezca el recorte a la región). Luego, es posible establecer la región a la resolución (y extensión) del mapa.
- @color[#8EA33B](r.out.\*) exportan mapas raster según la definición de la región computacional (extensión y resolución) y respetan la máscara si está presente. Se aplica interpolación por vecino más cercano por defecto.

@size[26px](@fa[exclamation-triangle text-orange] *En la importación y la exportación, los mapas vectoriales se consideran siempre en toda su extensión.*)

---

### Reportes y estadísticas
<br>

- [r.report](https://grass.osgeo.org/grass-stable/manuals/r.report.html): reporta área y número de celdas
- [r.coin](https://grass.osgeo.org/grass-stable/manuals/r.coin.html): reporta la matriz o tabla de coincidencia entre dos mapas raster

```bash
# Ejemplos
r.report map=zipcodes,landclass96 units=h,p
r.coin first=zipcodes second=landclass96 units=p

```

+++

- [r.univar](https://grass.osgeo.org/grass-stable/manuals/r.univar.html): calcula estadísticas descriptivas a partir de las celdas no nulas de un mapa raster
- [r.stats](https://grass.osgeo.org/grass-stable/manuals/r.stats.html): calcula el área de cada una de las categorías o intervalos de un mapa raster
- [r.statistics](https://grass.osgeo.org/grass-stable/manuals/r.statistics.html) y [r.stats.zonal](https://grass.osgeo.org/grass-stable/manuals/r.stats.zonal.html): estadística zonal
- [r.neighbors](https://grass.osgeo.org/grass-stable/manuals/r.neighbors.html): estadística local basada en las celdas vecinas

```bash
# estadísticas descriptivas
r.univar map=elevation

# estadística zonal: elevacion promedio por código postal
r.stats.zonal base=zipcodes cover=elevation method=average output=zipcodes_elev_avg
```

---

### Regresión simple y múltiple

- [r.regression.line](https://grass.osgeo.org/grass-stable/manuals/r.regression.line.html): regresión simple
- [r.regression.multi](https://grass.osgeo.org/grass-stable/manuals/r.regression.multi.html): regresión múltiple


```bash
# regresión simple
g.region raster=elev_srtm_30m -p
r.regression.line mapx=elev_ned_30m mapy=elev_srtm_30m 

# regresión múltiple
g.region raster=soils_Kfactor -p
r.regression.multi mapx=elevation,aspect,slope mapy=soils_Kfactor \
  residuals=soils_Kfactor.resid estimates=soils_Kfactor.estim
```

---

### @fa[calculator text-green] Álgebra de mapas raster @fa[calculator text-green]

<img src="assets/img/r_mapcalc_gui.png" width="50%">

[r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html) 

+++

#### Operadores

![Operators](assets/img/r_mapcalc_operators.png)

+++

#### Operador vecinos o índices **[row,col]**


```bash
# ejemplo filtro de paso bajo con operador vecinos
r.mapcalc \
expression="lsat7_2002_10_smooth = (lsat7_2002_10[-1,-1] + 
									lsat7_2002_10[-1,0] + 
									lsat7_2002_10[1,1] + 
									lsat7_2002_10[0,-1] + 
									lsat7_2002_10[0,0] + 
									lsat7_2002_10[0,1] + 
									lsat7_2002_10[1,-1] + 
									lsat7_2002_10[1,0] + 
									lsat7_2002_10[1,1]) / 9"
```

+++

<img src="assets/img/neighbour_operator_mapswipe.png" width="75%">

```bash
g.gui.mapswipe first=lsat7_2002_10 second=lsat7_2002_10_smooth
```

+++

#### Funciones

<img src="assets/img/r_mapcalc_functions.png" width="50%">

+++

#### Cláusula *if*

```bash
# Ejemplo: 
# Determinar las zonas forestales situadas por encima 
# de una cierta elevación

# establecer la región computacional
g.region rast=landclass96

# reportar las clases de cobertura
r.report map=landclass96 units=p

# estadística univariada del mapa de elevacion
r.univar map=elevation

# seleccionar áreas > 120m y con bosque
r.mapcalc expression="forest_high = if(elevation > 120 && landclass96 == 5, 1, null())"
```

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Object based image analysis - OBIA](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/05_obia&grs=gitlab)
<br><br>
@snapend


@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
