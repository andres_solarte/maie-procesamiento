---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---
@title[Sobre la disertante]

@snap[west]
@css[bio-about](- Lic. y Dra. Cs. Biológicas<br>- Mgter. en Aplicaciones Espaciales de Alerta y<br>Respuesta Temprana a Emergencias<br>- Aplicaciones de RS & GIS en<br>Eco-epidemiología<br>)
<br><br>
@css[bio-about](<a href="https://grass.osgeo.org/">GRASS GIS</a> Dev Team y project chair<br><a href="https://www.osgeo.org/">OSGeo</a> Charter member<br>FOSS4G enthusiast and advocate)
@snapend

@snap[east]
@css[bio-headline](About me)
<br><br>
![myphoto](assets/img/vero_round_small.png)
<br><br>
@css[bio-byline](<a href="https://github.com/veroandreo/">@fa[github pad-fa] veroandreo</a> <a href="https://twitter.com/VeronicaAndreo">@fa[twitter pad-fa] @VeronicaAndreo</a><br>@fa[globe pad-fa] https://veroandreo.gitlab.io)
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Introducción a GRASS GIS

---
@title[Intro FOSS]

@snap[north span-100]
<h2>Breve intro a los FOSS</h2>
<br><br>
@snapend

@snap[east split-screen-text]
Free and Open Source Software (FOSS) significa que **cualquiera** puede usar, copiar, 
estudiar, y modificar el software. El código fuente es abierto, se comparte 
gratuitamente, y las personas son libres de modificarlo y mejorarlo según sus necesidades.
@snapend

@snap[west split-screen-img]
<img src="assets/img/foss.png">
@snapend

---
@title[Intro OSGeo 1]

@snap[north span-100]
## Breve intro a OSGeo
<br><br>
@snapend

@snap[south span-100]
La Fundación [OSGeo](https://www.osgeo.org/) fue creada en 2006 para dar soporte al
desarrollo colaborativo de software para aplicaciones espaciales y promover su uso.
<br><br>
<img src="assets/img/osgeo-logo.png" width="50%">
<br><br>
@snapend

+++
@title[Intro OSGeo 2]

@snap[north span-100]
## Breve intro a OSGeo
@snapend

@snap[west list-content-verbose span-75 text-08]
<br><br>
@ol[](false)
- Los proyectos deben gestionarse por sí mismos, procurando el consenso y fomentando la participación de todos los colaboradores.
- Los colaboradores son un recurso escaso y los proyectos exitosos los cortejan y los alientan.
- Se alienta a los proyectos a adoptar estándares abiertos y a colaborar con otros proyectos de OSGeo.
- Los proyectos son responsables de revisar y controlar sus códigos fuente para asegurar su integridad.
@olend
<br><br>
@snapend

@snap[south-east span-25]
@img[](https://grass.osgeo.org/images/other/Grass_osgeo_codesprint_2018.jpg)
@snapend

---
@title[GRASS GIS history 1]

@snap[north span-100]
## GRASS GIS: Breve historia
<br><br>
@snapend

@snap[south-west list-content-verbose span-100]
@ul[](false)
- @color[#8EA33B](**GRASS GIS**) (Geographic Resources Analysis Support System), es un paquete de software libre y de código abierto utilizado para la gestión y análisis de datos geoespaciales, procesamiento de imágenes, producción de gráficos y mapas, modelado espacial y visualización. 
- Se utiliza en entornos académicos y comerciales de todo el mundo, y también en organismos gubernamentales.
- Originalmente desarrollado por los Laboratorios de Investigación de Ingeniería de la Construcción del Ejército de USA como una herramienta para la gestión del territorio y la planificación ambiental.
@ulend
<br>
@snapend

+++?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[GRASS GIS history 2]

@snap[north span-90]
<br>
Un poquito de historia (geek) sobre GRASS GIS...
@snapend

<iframe width="560" height="315" scrolling="no" src="//av.tib.eu/player/12963" frameborder="0" allowfullscreen></iframe>

@snap[south box-white text-07 span-80]
No se pierdan la página de [GRASS history](https://grass.osgeo.org/about/history/) y la lista de [releases](https://grass.osgeo.org/about/history/releases/) para más detalles y algunas anécdotas @fa[smile-wink text-orange]
@snapend

+++?color=linear-gradient(90deg, #8EA33B 50%, white 50%)
@title[Advantages and Disadvantages]

@snap[west text-white span-50]
Ventajas
<br><br>
@ul[split-screen-list](false)
- código abierto
- fuerte comunidad de usuarios, y apoyo comercial
- herramientas para datos raster/vectores 2D/3D, imágenes, espacio-temporales
- interfaz grafica GUI y linea de comandos CLI (facilita la escritura de rutinas)
- API y librerías de Python
@ulend
@snapend

@snap[east text-green span-50 text-left]
Desventajas
<br><br>
@ul[split-screen-list](false)
- el inicio es un tanto complicado en comparación con otros SIG [WIP]
- formato nativo (requiere la importación de datos, ofrece también la posibilidad de vincular formatos externos)
- datos vectoriales con topología (confuso para principiantes, a veces es difícil importar datos rotos)
@ulend
@snapend

+++?color=linear-gradient(90deg, #8EA33B 50%, white 50%)
@title[When to use and not to use GRASS]

@snap[west text-white span-50]
Cuándo usar GRASS GIS?
<br><br>
@ul[split-screen-list](false)
- análisis intensivo de datos geoespaciales
- trabajar con datos vectoriales topológicos
- analizar conjuntos de datos espacio-temporales
- integrar y escribir rutinas con Python
- desplegar aplicaciones del lado del servidor (ej., WPS)
@ulend
@snapend

@snap[east text-green span-50 text-left]
Cuándo usar otro SIG?
<br><br>
@ul[split-screen-list](false)
- para visualizar datos geográficos de forma fácil y rápida (mejor usar QGIS)
- si te asustan los *location* y *mapsets* @fa[smile-wink text-green]
@ulend
@snapend

---

@size[56px](Trabajar con GRASS GIS no es tan distinto a trabajar con otros SIG...)

---

Bueno, salvo por esto... @fa[surprise text-orange]

@img[width=500px](assets/img/start_screen1.png)

---

## Nociones básicas

@ul
- La @color[#8EA33B](**BASE DE DATOS GRASS o GRASS DATABASE**) (también llamada "GISDBASE") es un directorio, usualmente llamado `grassdata` que contiene todos nuestros *LOCATIONs*
- Un @color[#8EA33B](**LOCATION**) se define por su sistema de coordenadas.
- Un @color[#8EA33B](**MAPSET**) es un subdirectorio dentro de un *Location*, equivalente a la noción de proyecto en otros GIS. Los **MAPSETs** pueden hacer referencia a distintos temas, regiones, etc.
@ulend

+++
@title[GRASS DB, Location y Mapsets]

Para iniciar GRASS GIS se necesita especificar una ruta compuesta de *Database*, *Location* y *Mapset*

<img src="assets/img/grass_database.png" width="65%">

@size[24px](<a href="https://grass.osgeo.org/grass-stable/manuals/grass_database.html">GRASS database</a>)
<br><br>

+++

- **Por qué esta estructura?**

 - GRASS GIS tiene un @color[#8EA33B](*formato nativo*) para los datos raster y vectoriales, por lo tanto
   estos deben ser *importados* or *vinculados* a un Location y Mapset (ver [r.external](https://grass.osgeo.org/grass-stable/manuals/r.external.html) por ejemplo).

+++

- **Cuáles son las ventajas?**

 - GRASS DATABASE, LOCATIONs y MAPSETs son directorios 
 @color[#8EA33B](*que pueden ser fácilmente compartidos con otros usuarios*).
 - La base de datos de GRASS (`grassdata`) puede ser @color[#8EA33B](*local o remota*), y permite configurar @color[#8EA33B](*permisos especiales*) para cada mapset en un LOCATION.
 - Todos los mapas dentro de un LOCATION tienen necesariamente @color[#8EA33B](el mismo sistema de coordenadas).

+++?color=linear-gradient(90deg, #8EA33B 46%, white 46%)

@snap[west split-screen-heading text-white span-45]
Tipos de datos en GRASS GIS
@snapend

@snap[east span-55]
@ul[split-screen-list](false)
- [Raster](https://grass.osgeo.org/grass-stable/manuals/rasterintro.html) (incluyendo [imágenes satelitales](https://grass.osgeo.org/grass-stable/manuals/imageryintro.html))
- [Raster 3D o voxel](https://grass.osgeo.org/grass-stable/manuals/raster3dintro.html)
- [Vector](https://grass.osgeo.org/grass-stable/manuals/vectorintro.html): punto, línea, límite o borde, área, caras
- [Espacio-temporales](https://grass.osgeo.org/grass-stable/manuals/temporalintro.html): colecciones de datos raster (**STRDS**), raster 3D (**STR3DS**) o vectores (**STVDS**)
@ulend
@snapend

---

## @fa[tools text-green] Módulos @fa[tools text-green]

Más de [500 módulos](https://grass.osgeo.org/grass-stable/manuals/full_index.html) para las más variadas tareas, pero con una organización clara:

| Prefix                                                               | Function class   | Type of command                     | Example
|--------------------------------------------------------------------- |:---------------- |:----------------------------------- |:-------------------------------------------------------------------------------------------------------------------
| [g.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#g)    | general          | general data management             | [g.rename](https://grass.osgeo.org/grass-stable/manuals/g.rename.html): renames map
| [d.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#d)    | display          | graphical output                    | [d.rast](https://grass.osgeo.org/grass-stable/manuals/d.rast.html): display raster map 
| [r.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#r)    | raster           | raster processing                   | [r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html): map algebra
| [v.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#r)    | vector           | vector processing                   | [v.clean](https://grass.osgeo.org/grass-stable/manuals/v.clean.html): topological cleaning
| [i.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#i)    | imagery          | imagery processing                  | [i.pca](https://grass.osgeo.org/grass-stable/manuals/i.pca.html): Principal Components Analysis on imagery group
| [r3.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#r3)  | voxel            | 3D raster processing                | [r3.stats](https://grass.osgeo.org/grass-stable/manuals/r3.stats.html): voxel statistics
| [db.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#db)  | database         | database management                 | [db.select](https://grass.osgeo.org/grass-stable/manuals/db.select.html): select value(s) from table
| [ps.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#ps)  | postscript       | PostScript map creation             | [ps.map](https://grass.osgeo.org/grass-stable/manuals/ps.map.html): PostScript map creation
| [t.\*](https://grass.osgeo.org/grass-stable/manuals/full_index.html#t)    | temporal         | space-time datasets                 | [t.rast.aggregate](https://grass.osgeo.org/grass-stable/manuals/t.rast.aggregate.html): raster time series aggregation

+++

<img src="assets/img/module_tree_and_search.png" width="80%">
<br>
Árbol de módulos y motor de búsqueda

---

## @fa[plug text-green] Add-ons o extensiones @fa[plug text-green]

Las extensiones o **add-ons** pueden ser instaladas desde el
[repositorio central](https://grass.osgeo.org/grass7/manuals/addons/) 
o desde *GitHub* (u otros similares) usando el comando 
[g.extension](https://grass.osgeo.org/grass-stable/manuals/g.extension.html)

```bash
 # instalar una extensión desde el repositorio de GRASS GIS
 g.extension extension=r.hants
 
 # instalar una extensión desde un repositorio github
 g.extension extension=r.in.sos \
   url=https://github.com/pesekon2/GRASS-GIS-SOS-tools/tree/master/sos/r.in.sos
``` 

+++

## @fa[plug text-green] Add-ons o extensiones @fa[plug text-green]

@color[#8EA33B](r.tuaddon.here)

- Si tenés conocimientos de programación o no, pero te gusta el software de código abierto y GRASS GIS, no dudes en [contribuir](https://github.com/veroandreo/grass/blob/master/CONTRIBUTING.md)!

---

## Región computacional

![Show computational region](assets/img/region.png)

@size[18px](Para más detalles ver la wiki sobre [Región computacional](https://grasswiki.osgeo.org/wiki/Computational_region))

+++

@snap[west]
@ul[]
- La @color[#8EA33B](**región computacional**) es la configuración de límites del área de análisis y resolución espacial (raster).
- La @color[#8EA33B](**región computacional**) puede ser definida y modificada con el comando [g.region](https://grass.osgeo.org/grass-stable/manuals/g.region.html) a la extensión de un mapa vectorial, un raster o manualmente a algún area de interés.
- Los mapas raster de salida *(output)* tendrán una extensión y resolución espacial igual a la región computacional, mientras que los mapas vectoriales son siempre procesados en su extensión original.
@ulend
@snapend

+++

## Región computacional

- **Cuáles son las ventajas?**

  - Mantener los resultados consistentes
  - Evitar recortar los mapas antes del análisis de sub-áreas
  - Probar un algoritmo o proceso computacional exigente (time consuming) en áreas pequeñas
  - Ajustar la configuración o parámetros de un determinado módulo
  - Ejecutar diferentes procesos en diferentes áreas

---

## Interfaces

GRASS GIS ofrece diferentes interfaces para la interacción entre usuarios y software 

#### Veamos cada una de ellas!
<br>
@fa[laptop fa-2x text-green]

+++

### Interfaz Gráfica de Usuario (GUI)

![GRASS GIS GUI](assets/img/GUI_description.png)

+++

algunos cambios en camino...

@img[width=900px](assets/img/new_gui_grass79dev.png)

+++

### @fa[terminal] Línea de Comandos o terminal 

<br>
<img src="assets/img/grass_command_line.png" width="70%">

+++

### Ventajas de la línea de comandos

@ul
- Ejecutar *`history`* para ver todos los comandos anteriores
- La historia se almacena individualmente por MAPSET
- Buscar en la historia con `<CTRL-R>`
- Guardar los comandos en un archivo: `history > my_protocol.sh`, pulir/anotar el protocolo y volver a ejecutar con: `sh my_protocol.sh`
- Llamar la GUI del módulo y "Copiar" el comando para su posterior replicación.
@ulend

+++

La línea de comandos simple de la GUI también ofrece un botón *Log file* 
para guardar la historia de comandos

<img src="assets/img/command_prompt_gui.png" width="43%">

+++

### @fab[python text-green] Python @fab[python text-green]

La forma más sencilla de ejecutar una rutina de Python en GRASS GIS, es a través del *Simple Python editor*

<img src="assets/img/simple_python_editor.png" width="80%">

+++

... o simplemente se puede escribir la rutina en un editor de texto y ejecutarla desde la terminal o la GUI:

```python
 #!/usr/bin/env python

 # simple example for pyGRASS usage: raster processing via modules approach
 from grass.pygrass.modules.shortcuts import general as g
 from grass.pygrass.modules.shortcuts import raster as r
 g.message("Filter elevation map by a threshold...")
 
 # set computational region
 input = 'elevation'
 g.region(raster=input)
 output = 'elev_100m'
 thresh = 100.0

 r.mapcalc("%s = if(%s > %d, %s, null())" % (output, input, thresh, input), overwrite = True)
 r.colors(map=output, color="elevation")
``` 

+++

... o por medio de la librería [**grass-session**](https://github.com/zarch/grass-session)

@code[python](code/01_intro_grass_session_vector_import.py)

@[17-28](Importar librerías)
@[36-48](Crear Location y Mapset)
@[50-66](Ejecutar los comandos)
@[68-69](Limpiar y cerrar)

+++

... o con Jupyter notebooks

<img src="assets/img/jupyter_notebook_locally.png" width="70%">

@size[18px](Para más ejemplos ver la <a href="https://grasswiki.osgeo.org/wiki/GRASS_GIS_Jupyter_notebooks#List_of_selected_GRASS_GIS_Jupyter_notebooks">lista de Jupyter notebooks</a> que usan GRASS)

+++

@img[span-40](https://upload.wikimedia.org/wikipedia/commons/c/c2/QGIS_logo%2C_2017.svg)

Hay dos formas de utilizar las funciones de GRASS GIS dentro de QGIS:
<br>
- [GRASS GIS plugin](https://docs.qgis.org/3.14/en/docs/user_manual/grass_integration/grass_integration.html)
- [Processing toolbox](https://docs.qgis.org/3.14/en/docs/user_manual/processing/toolbox.html)

+++

![GRASS GIS modules through GRASS Plugin](assets/img/qgis_grass_plugin.png)
<br>
@size[18px](Usando GRASS GIS a través del *GRASS GIS plugin*)

+++

![GRASS modules through Processing Toolbox](assets/img/qgis_processing.png)
<br>
@size[18px](Usando GRASS GIS a través del *Processing Toolbox*)

+++

### @fab[r-project text-green] R + rgrass7 @fab[r-project text-green]

GRASS GIS y R se pueden usar juntos de dos maneras:
<br><br>
- [R dentro de una sesión de GRASS GIS](https://grasswiki.osgeo.org/wiki/R_statistics/rgrass7#R_within_GRASS)
- [GRASS GIS dentro de una sesión de R](https://grasswiki.osgeo.org/wiki/R_statistics/rgrass7#GRASS_within_R)
<br><br>

@size[22px](Detalles y ejemplos en la wiki <a href="https://grasswiki.osgeo.org/wiki/R_statistics/rgrass7">GRASS y R</a>)

+++

![Calling R from within GRASS](assets/img/RwithinGRASS_and_Rstudio_from_grass.png)

+++

### WPS - OGC Web Processing Service

- [Web Processing Service](https://en.wikipedia.org/wiki/Web_Processing_Service) 
es un standard de la [OGC](https://en.wikipedia.org/wiki/Open_Geospatial_Consortium). 
- [ZOO-Project](http://zoo-project.org/) y [PyWPS](http://pywps.org/) son interfaces 
que permiten ejecutar comandos de GRASS GIS de manera simple desde la web.

+++

### GRASS in the cloud: [*actinia*](https://actinia.mundialis.de/)

- API REST para procesamiento escalable, distribuido y de alto rendimiento 
- Utiliza principalmente GRASS para tareas de computación
- Sigue el paradigma de llevar algoritmos a los geodatos almacenados en la nube 
- Disponible en [GitHub](https://github.com/mundialis/actinia_core)
- OSGeo Community project desde 2019. 

@snap[south-east span-15]
![logo actinia](assets/img/actinia.png)
<br><br><br>
@snapend

@size[20px](Para más info, ver el <a href="https://neteler.gitlab.io/actinia-introduction/">Tutorial sobre Actinia</a> en la Escuela de verano OpenGeoHub 2019)

+++

### @fa[globe text-green] GRASS GIS online @fa[globe text-green]

<img src="assets/img/try_grass_online_jupyter.png" width="70%">

@size[20px](Para más detalles, ver https://github.com/wenzeslaus/try-grass-in-jupyter)

---

## Comandos frecuentes y algo más

+++

- [r.import](https://grass.osgeo.org/grass-stable/manuals/r.import.html) y
  [v.import](https://grass.osgeo.org/grass-stable/manuals/v.import.html):
  importan mapas de tipo raster y vectorial con re-proyección, recorte
  y remuestreo al vuelo.

```bash 
 ## IMPORT RASTER DATA: SRTM V3 data for NC
 
 # set computational region to e.g. 10m elevation model:
 g.region raster=elevation -p
 
 # Import with reprojection on the fly
 r.import input=n35_w079_1arc_v3.tif output=srtmv3_resamp10m \
  resample=bilinear extent=region resolution=region \
  title="SRTM V3 resampled to 10m resolution"

 ## IMPORT VECTOR DATA
 
 # import SHAPE file, clip to region extent and reproject to 
 # current location projection
 v.import input=research_area.shp output=research_area extent=region
``` 

+++

- [g.list](https://grass.osgeo.org/grass-stable/manuals/g.list.html): lista
  el o los tipos de elementos/datos elegidos por el usuario (i.e., 
  raster, vector, raster 3D, region, labels). Opcionalmente permite
  usar patrones y expresiones regulares para realizar la búsqueda y listado.

```bash 
 g.list type=vector pattern="r*"
 g.list type=vector pattern="[ra]*"
 g.list type=raster pattern="{soil,landuse}*"
``` 

+++

- [g.remove](https://grass.osgeo.org/grass-stable/manuals/g.remove.html),
  [g.rename](https://grass.osgeo.org/grass-stable/manuals/g.rename.html)
  y [g.copy](https://grass.osgeo.org/grass-stable/manuals/g.copy.html):

  Estos módulos eliminan, renombran o copian mapas de la base de datos
  ya sea dentro de un mapset o desde otros mapsets.
  
> @fa[exclamation-triangle text-orange] Estas tareas deben **SIEMPRE** realizarse desde dentro de GRASS

+++

- [g.region](https://grass.osgeo.org/grass-stable/manuals/g.region.html):
  Maneja los límites y resolución de la región computacional.

```bash 
 ## Subset a raster map
 # 1. Check region settings
 g.region -p
 # 2. Change region (here: relative to current N and W values, expanding values in map units)
 g.region n=n-3000 w=w+4000
 # 3. Subset map
 r.mapcalc "new_elev = elevation"
 r.colors new_elev color=viridis
 # 4. Display maps
 d.mon wx0
 d.rast elevation
 d.rast new_elev
``` 

+++

- [g.mapset](https://grass.osgeo.org/grass-stable/manuals/g.mapset.html)
  y [g.mapsets](https://grass.osgeo.org/grass-stable/manuals/g.mapsets.html):
  Estos módulos permiten cambiar de mapset y agregar  o remover mapsets a 
  la lista de mapsets accesibles.

```bash
 # print current mapset
 g.mapset -p
 # change to a different mapset
 g.mapset mapset=modis_lst
 # print mapsets in the search path
 g.mapsets -p
 # list available mapsets in the location
 g.mapsets -l
 # add mapset to the search path
 g.mapsets mapset=modis_lst operation=add
``` 

+++

- [r.info](https://grass.osgeo.org/grass-stable/manuals/r.info.html) y
  [v.info](https://grass.osgeo.org/grass-stable/manuals/v.info.html):
  Son módulos muy útiles para obtener información básica sobre los mapas
  como también su historia.

```bash
 # info for raster map
 r.info elevation
 # info for vector map
 v.info nc_state
 # history of vector map
 v.info nc_state -h
```

+++

- [--exec en el comando de inicio de GRASS GIS](https://grass.osgeo.org/grass-stable/manuals/grass7.html): 
  Esta opción permite ejecutar módulos o rutinas de trabajo escritas en bash o Python sin la necesidad
  de iniciar GRASS GIS (i.e., sin abrir el software).

```bash
 # running a module
 grass78 /path/to/grassdata/nc_spm_08_grass7/PERMANENT/ --exec r.univar map=elevation
 
 # running a script
 grass78 /path/to/grassdata/nc_spm_08_grass7/PERMANENT/ --exec sh test.sh

 ## test.sh might be as simple as:
 
 #!/bin/bash

 g.region -p
 g.list type=raster
 r.info elevation
``` 

---

# @fa[ambulance text-pink] **AYUDA!!!** @fa[ambulance text-pink]

+++

### KEEP CALM and GRASS GIS

- [g.manual](https://grass.osgeo.org/grass-stable/manuals/g.manual.html):
  en la GUI bajo el menú de "Ayuda" o simplemente presionando <F1>
- `--help` o `--h` + nombre del módulo en la terminal
- [GRASS wiki](https://grasswiki.osgeo.org/wiki/GRASS-Wiki): ejemplos,
  explicaciones y ayuda sobre módulos o tareas particulares,
  [tutoriales](https://grasswiki.osgeo.org/wiki/Category:Tutorial),
  aplicaciones, noticias, etc.
- [Jupyter/IPython notebooks](https://grasswiki.osgeo.org/wiki/GRASS_GIS_Jupyter_notebooks)
  con ejemplos de rutinas para diferentes aplicaciones
- lista de mails grass-user: [subscribite](https://lists.osgeo.org/mailman/listinfo/grass-user) y
  envía tu consulta o revisa los [archivos](https://lists.osgeo.org/pipermail/grass-user/)

+++

## @fa[bookmark text-green] Enlaces (muy) útiles @fa[bookmark text-green]

- [GRASS intro workshop en NCSU](https://ncsu-osgeorel.github.io/grass-intro-workshop/)
- [Unleash the power of GRASS GIS en US-IALE 2017](https://grasswiki.osgeo.org/wiki/Unleash_the_power_of_GRASS_GIS_at_US-IALE_2017)
- [GRASS GIS workshop en Jena 2018](http://training.gismentors.eu/grass-gis-workshop-jena-2018/index.html)
- [Raster data processing in GRASS GIS](https://grass.osgeo.org/grass-stable/manuals/rasterintro.html)
- [Vector data processing in GRASS GIS](https://grass.osgeo.org/grass-stable/manuals/vectorintro.html)

---

## @fa[book text-green] Referencias @fa[book text-green]

- Neteler, M., Mitasova, H. (2008) *Open Source GIS: A GRASS GIS Approach*. Third edition. ed. Springer, New York. [Book site](https://grassbook.org/)
- Neteler, M., Bowman, M.H., Landa, M. and Metz, M. (2012) *GRASS GIS: a multi-purpose Open Source GIS*. Environmental Modelling & Software, 31: 124-130 [DOI](http://dx.doi.org/10.1016/j.envsoft.2011.11.014)

---

@snap[north-west]
**Proximamente...**
@snapend

<br>
<a href="https://wenzeslaus.github.io/grass-gis-talks/ncgis2021.html#/"><img src="assets/img/grass8.png" width="60%"></a>

@size[22px](Presentation by Vaclav Petras & Anna Petrasova, NCGIS 2021) 

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---?image=assets/img/grass_sprint2018_bonn_fotowall_medium.jpg&size=cover

@transition[zoom]

<p style="color:white">Join and enjoy GRASS GIS!!</p>

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Un paseo por las funciones de GRASS GIS](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/02_general_intro_capabilities&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
