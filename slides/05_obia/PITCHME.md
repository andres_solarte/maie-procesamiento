---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Object based image analysis (OBIA)

<br>
@fa[home text-gray fa-3x] @fa[industry text-gray fa-3x] @fa[building text-gray fa-3x]
<br><br>

---

@snap[north-west span-60]
<h3>Contenidos</h3>
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Nociones básicas sobre OBIA
- Flujo de trabajo en OBIA
- Segmentación
- Optimización de parámetros de segmentación
- Generación de capas de información
- Estadística de segmentos
- Clasificación de segmentos
- Módulos y extensiones de GRASS para OBIA 
@olend
@snapend

---

@snap[north span-100]
Análisis de imágenes
<br>
@snapend

@snap[west span-50]
<br>
![](assets/img/rgb_cba.png)
Datos
@snapend

@snap[east span-50]
<br>
![](assets/img/obia_cba.png)
Información
@snapend

---

@snap[north span-100]
Alta resolución (HR) vs Muy alta resolución (VHR)
@snapend

@snap[midpoint span-100]
![width=640px](assets/img/HR_VHR.png)
@snapend

@snap[south-west span-50 text-04]
@ol[](false)
- pixel 10 x 10 m
- libre acceso
- mayor res espectral
- mayor res temporal
- mayor extensión
@olend
@snapend

@snap[south-east span-50 text-04]
@ol[](false)
- pixel 0.2 x 0.2m
- (alto) costo
- menor res espectral
- menor re temporal
- menor extension
@olend
@snapend

+++

@snap[north span-100]
#### Alta resolución (HR) vs Muy alta resolución (VHR)
@snapend

@snap[west span-33 text-05]
![20m](assets/img/20m_pix.png)
@ul[](false)
- Varios objetos geográficos en el mismo pixel
- Respuesta espectral mixta, no hay delineación clara de los objetos
@ulend
@snapend

@snap[midpoint span-33 text-05]
![5m](assets/img/5m_pix.png)
@ul[](false)
- Algunos píxeles de respuesta espectral mixta y algunos puros
- Resolución espacial ~ tamaño de algunos objetos geográficos
@ulend
@snapend

@snap[east span-33 text-05]
![125cm](assets/img/125cm_pix.png)
@ul[](false)
- La mayoría de los píxeles pertenecen a una única clase 
- Un solo objeto geográfico contiene varios píxeles
@ulend
@snapend

@snap[south span-100 text-05]
*Blaschke 2010. “Object Based Image Analysis for Remote Sensing.” ISPRS Journal of Photogrammetry and Remote Sensing 65:2-16.*
@snapend

---
@snap[north span-100]
### OBIA: Object-based image analysis
<br>
Idea general: agrupar los píxeles en objetos antes de clasificarlos
<br><br>
@snapend

@snap[south-west message-box span-45 text-08]
<br>
**Ventajas:**
@ul[](false)
- Se consideran las características de los objetos, no los píxeles
- Salida más suave (sin efecto "sal y pimienta")
@ulend
<br><br>
@snapend

@snap[south-east message-box span-45 text-08 text-left]
<br>
**Dificultades:**
@ul[](false)
- Necesidad de identificar primero los "objetos"
- Necesidad de definir la "mejor" delimitación de los objetos
@ulend
<br><br>
@snapend

+++

@img[width=750px](assets/img/salt_pepper.png)

@size[24px](Ejemplo del efecto *sal y pimienta*)

---

### Etapas en la aproximación OBIA

- Pre-procesamiento
- Segmentación
- Obtención de estadística de los objetos
- (Selección de *features*)
- Clasificación
- Post-clasificación

---

### Etapas en la aproximación OBIA

- Pre-procesamiento
- @color[#8EA33B](**Segmentación**)
- @color[#8EA33B](**Obtención de estadística de los objetos**)
- (Selección de *features*)
- @color[#8EA33B](**Clasificación**)
- Post-clasificación

---

### Segmentación

- *¿Qué es?*
  - Agrupación de píxeles en objetos

- *¿Cómo deberían ser los resultados?*
  - La mejor aproximación/delineación posible de los objetos reales
  - Heterogeneidad espectral dentro de los objetos (ej.: 1/2 techo en el sol, 1/2 en la sombra)

+++

- Es necesaria una **buena segmentación para una buena clasificación**
  - Una mala segmentación puede afectar dramáticamente los resultados de la clasificación
  - La segmentación *óptima* no es necesariamente la misma en toda la imagen

+++

Sobre-segmentación, sub-segmentación y trade-off

@img[width=950px](assets/img/ejemplo_sub_sobre_segm.png)

+++

#### Cómo seleccionar los parámetros *óptimos*

- Manualmente, **ensayo y error** para ajustar iterativamente los parámetros y producir los segmentos esperados
- Optimización **automatizada**
  - *Supervisada*: con polígonos de referencia @fa[arrow-right text-green] Requiere conocimiento *a priori*
  - *No supervisada*: sólo confiando en las estadísticas de la imagen @fa[arrow-right text-green] No requiere conocimiento *a priori*

+++

#### Observaciones importantes

- No hay una segmentación perfecta: **es siempre un compromiso**
- Preferencia por objetos sobre-segmentados sobre aquellos sub-segmentados
 - Los objetos sub-segmentados contienen diferentes clases de cubierta terrestre @fa[arrow-right text-green] difíciles de clasificar
 - Los objetos sobre-segmentados pueden ser fusionados durante la clasificación

---

### Segmentación en GRASS GIS

[i.segment](https://grass.osgeo.org/grass-stable/manuals/i.segment.html)

@img[width=700px](assets/img/i_segment.png)

+++

### Region growing 

- Dos parámetros:
  - threshold: similitud espectral entre segmentos adyacentes
  - minsize: tamaño mínimo (# de píxeles) del segmento en la salida final

+++

@img[width=700px](assets/img/region_growing.jpg)

@size[24px](Diagrama explicativo del algoritmo *region growing*)

+++

### Mean shift

- 3 parámetros y 2 opciones:
  - threshold: similitud espectral entre segmentos adyacentes
  - hr: rango, sólo las celdas dentro del rango se consideran para realizar el mean shift
  - radius: radio espacial en número de celdas
  - -a usar rango adaptable para mean shift
  - -p usar radio progresivo para mean shift

+++

@img[width=700px](assets/img/mean_shift.gif)

+++

### Segmentación en GRASS GIS

- La salida de la segmentación es en formato raster
  - El valor de los píxeles corresponde al ID del segmento (único)
  - Puede transformarse en vector con el módulo [r.to.vect](https://grass.osgeo.org/grass-stable/manuals/r.to.vect.html)

@img[width=300px](assets/img/i_segment.png)

---

### Unsupervised Segmentation Parameter Optimization - USPO

- Permite la selección automatizada del **"mejor"** valor para un parámetro entre una serie de valores
- Basado en el cálculo de las estadísticas de los objetos
  - Varianza ponderada (homogeneidad intra-segmentos)
  - Autocorrelación espacial (heterogeneidad entre segmentos)

+++

@snap[north span-100]
### USPO en GRASS GIS

[i.segment.uspo](https://grass.osgeo.org/grass7/manuals/addons/i.segment.uspo.html)
@snapend

@snap[west span-50]
<br><br>
@img[width=400px](assets/img/uspo.png)
@snapend

@snap[east span-50 text-left text-08]
<br>
@ul[](false)
- Primero identificar los valores extremos (sobre y sub-segmentación) @fa[arrow-right] rango de valores a evaluar
- Con capacidad de hardware limitada, no probar demasiadas combinaciones y trabajar en una región limitada
@ulend
@snapend

+++

### USPO en GRASS GIS

- Salida de *i.segment.uspo*: la selección de los mejores valores de los parámetros que se introducirán en *i.segment*

@img[width=700px](assets/img/terminal_uspo.png)

- Opcionalmente, los primeros X mejores resultados pueden guardarse directamente como capas raster

+++

*i.segment.uspo* pedirá los nombres de un grupo de imágenes y regiones
<br><br>

> @fa[question-circle text-orange] Qué módulos usábamos para esas tareas?

+++

Para grandes conjuntos de datos, *i.segment.uspo* se puede usar para segmentar automáticamente tiles pequeños, cada uno con un parámetro optimizado

@img[width=600px](assets/img/cutlines.png)

[i.cutlines](https://grass.osgeo.org/grass-stable/manuals/addons/i.cutlines.html)

---

### Obtención de información derivada

- Índices de vegetación: [i.vi](https://grass.osgeo.org/grass-stable/manuals/i.vi.html) o [r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html)
- Índices de agua: [i.wi](https://grass.osgeo.org/grass-stable/manuals/addons/i.wi.html) o [r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html)
- Texturas: [r.texture](https://grass.osgeo.org/grass-stable/manuals/r.texture.html) o [r.texture.tiled](https://grass.osgeo.org/grass-stable/manuals/addons/r.texture.tiled.html)
- etc.

@img[width=500px](assets/img/ndvi_ndwi.png)

+++
 
#### GLCM textures

- Grey-level co-occurrence matrix
- Importante fuente de información
- En el entorno urbano, utilizando imágenes de resolución espacial media, ayuda a identificar las estructuras/funciones urbanas (por ejemplo, los asentamientos informales)

@img[width=350px](/assets/img/vhr_pan.png) 
@img[width=350px](/assets/img/spot_texture.png)

---

### Estadística de objetos

[i.segment.stats](https://grass.osgeo.org/grass-stable/manuals/addons/i.segment.stats.html)

A efectos de la clasificación, necesitamos calcular las características de los objetos (estadísticas)
- valores raster (bandas, índices espectrales, texturas)
- forma del objeto (área, perímetro, compact_circle, compact_square, fd)
- objetos del vecindario (raster stats y shape stats of all neighbours)

---

### Clasificación supervisada

- **Basada en reglas**
  - Reglas estadísticas y valores de umbral definidos por un experto
  - Difícil encontrar reglas que funcionen en todas partes de la imagen

+++

### Clasificación supervisada

- **Basada en modelos** (ej. Random Forest, SVM, ANN)
  - Clasificador entrenado usando una muestra de entrenamiento de objetos
  - El clasificador encuentra las características con mayor poder de predicción y asigna clase a todos los objetos no etiquetados

+++

#### Clasificación basada en reglas en GRASS GIS

- Actualizar una columna en la tabla de atributos de una capa de vectores con [v.db.update](https://grass.osgeo.org/grass-stable/manuals/addons/v.db.update.html) o [db.execute](https://grass.osgeo.org/grass-stable/manuals/addons/db.execute.html)

@img[width=400px](assets/img/sql_rules.png)

- Reclasificar un raster resultante de la segmentación con [r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html)

+++

#### Clasificación por machine learning en GRASS GIS

[v.class.mlR](https://grass.osgeo.org/grass-stable/manuals/addons/v.class.mlR.html)

- Utiliza el paquete *caret* de @fab[r-project fa-2x text-blue]
- Varios clasificadores (Random Forest, SVM, KNN)
- Selección de variables/features
- Opción de combinar los resultados de distintos clasificadores a través de *majority vote*

<br>
@fa[exclamation text-orange] @size[22px](Para datos raster, la extensión <a href="https://grass.osgeo.org/grass-stable/manuals/addons/r.learn.ml.html">r.learn.ml</a> nos permite usar algoritmos de aprendizaje automático para realizar clasificaciones supervisadas)

---

@snap[north span-100]
### OBIA en GRASS GIS
@snapend

@snap[west span-50 text-center]
<br>
![width=350px](assets/img/obia_flow.png)
@snapend

@snap[east span-50 text-left text-05]
<br>
@ul[](false)
- [i.segment](https://grass.osgeo.org/grass-stable/manuals/i.segment.html) para segmentación
- [i.segment.uspo](https://grass.osgeo.org/grass-stable/manuals/addons/i.segment.uspo.html) para optimización no supervisada de los parámetros de segmentación
- [i.segment.stats](https://grass.osgeo.org/grass-stable/manuals/addons/i.segment.stats.html) para computar estadísticas de los segmentos
- [i.superpixels.slic](https://grass.osgeo.org/grass7/manuals/addons/i.superpixels.slic.html) para segmentación de superpixeles (SLIC y SLIC0)
- [r.to.vect](https://grass.osgeo.org/grass-stable/manuals/r.to.vect.html) para convertir de raster a vector
- [db.execute](https://grass.osgeo.org/grass-stable/manuals/db.execute.html) para ejecutar consultas SQL y actualizar valores en la tabla de atributos de las capas vectoriales (clasificación basada en reglas)
- [r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html) para cálculos raster, ej. clasificación basada en reglas
- [v.class.mlR](https://grass.osgeo.org/grass7/manuals/addons/v.class.mlR.html) para el aprendizaje automático, basado en *caret* de R
- [i.cutlines](https://grass.osgeo.org/grass7/manuals/addons/i.cutlines.html) para la partición automatizada (tiling) de una gran escena por líneas naturales
@ulend
@snapend

---

<img src="assets/img/gummy-question.png" width="45%">

---

## @fa[book text-green] Referencias @fa[book text-green]

- Blaschke, T. (2010). Object based image analysis for remote sensing. ISPRS JPRS, 65(1), 2–16. [DOI](https://doi.org/10.1016/j.isprsjprs.2009.06.004)
- Grippa, T. et al. (2017). An Open-Source Semi-Automated Processing Chain for Urban Object-Based Classification. Remote Sensing, 9, 358. [DOI](https://doi.org/10.3390/rs9040358)
- Georganos, S. et al. (2018). Scale Matters: Spatially Partitioned Unsupervised Segmentation Parameter Optimization for Large and Heterogeneous Satellite Images. Remote Sensing, 10, 1440. [DOI](http://dx.doi.org/10.3390/rs10091440)

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Ejercicio: Clasificación basada en objetos con datos SPOT](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=exercises/04_obia&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
