---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## GRASS GIS: Un paseo por sus funcionalidades
<br>

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Interoperabilidad

<br>

+++

<img src="assets/img/grass_database_vs_geodata.png" width="90%">

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)
@title[Raster Import/Export]

@snap[north text-white span-100]
@fa[file-import] Importar y exportar mapas raster @fa[file-export]
@snapend

@snap[west span-50]
<img src="assets/img/File_raster_import.png">
@snapend

@snap[east span-50]
<img src="assets/img/File_raster_export.png">
@snapend

@snap[south span-100]
GRASS se apoya en [GDAL](https://www.gdal.org/) para importar y exportar mapas raster
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)
@title[Vector Import/Export]

@snap[north text-white span-100]
@fa[file-import] Importar y exportar mapas vectoriales @fa[file-export]
@snapend

@snap[west span-50]
<img src="assets/img/File_vector_import.png">
@snapend

@snap[east span-50]
<img src="assets/img/File_vector_export.png">
@snapend

@snap[south span-100]
GRASS se apoya en [OGR](https://www.gdal.org/ogr_arch.html) para importar y exportar datos vectoriales
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos raster

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Menú Raster
@snapend

@snap[west span-50]
<br>
<img src="assets/img/Raster_menu.png" width="90%">
@snapend

@snap[east span-50]
@ul[header-footer-list-shrink](false)
- Datos raster: DEM, cobertura, clima, etc.
- Imágenes satelitales: Landsat, Sentinel, MODIS, SPOT, QuickBird, etc.
@ulend
@snapend

@snap[south span-100]
[Raster processing](https://grass.osgeo.org/grass-stable/manuals/rasterintro.html) manual
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Remuestreo
@snapend

@snap[west span-50]
@ul[header-footer-list-shrink](false)
- [r.resamp.interp](https://grass.osgeo.org/grass-stable/manuals/r.resamp.interp.html): Remuestrea el mapa raster a una cuadrícula más fina usando diferentes métodos de interpolación: vecino más cercano, bilinear, bicúbico (downscaling)
- [r.resamp.stats](https://grass.osgeo.org/grass-stable/manuals/r.resamp.stats.html): Remuestrea el mapa raster a una cuadrícula más gruesa utilizando agregación (upscaling)
@ulend
<br>
@snapend

@snap[east span-50]
<img src="assets/img/Raster_resample_options.png">
<br>
@snapend

@snap[south span-60]
<img src="assets/img/r_resamp_stats_6m_20m.png" width="60%">
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Superposición de mapas raster
@snapend

@snap[west span-50]
<img src="assets/img/Raster_overlay_options.png">
<br><br>
@snapend

@snap[south span-60]
<img src="assets/img/r_patch.png" width="70%">
@snapend

@snap[east span-50]
@ul[header-footer-list-shrink](false)
- [r.series](https://grass.osgeo.org/grass-stable/manuals/r.series.html): Permite agregar una lista de mapas con diferentes métodos como promedio, mínimo, máximo, etc.
- [r.patch](https://grass.osgeo.org/grass-stable/manuals/r.patch.html): Crea un mapa raster utilizando los valores de las categorías de uno (o más) mapa(s) para rellenar las áreas "sin datos" en otro mapa
@ulend
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Modelado hidrológico
@snapend

@snap[west span-50]
<img src="assets/img/Raster_hydro.png">
@snapend

@snap[east span-50]
@size[20px](... además de muchos otros add-ons, por ejemplo:)

<img src="assets/img/r_stream_addons.jpg">

@size[20px](<a href="https://doi.org/10.1016/j.cageo.2011.03.003">Jasiewics & Metz, 2011</a>)
@snapend

@snap[south span-70]
<img src="assets/img/r_stream_collage.png" width="75%">
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Análisis del terreno
@snapend

@snap[west span-50]
<img src="assets/img/Raster_terrain_analysis.png">
@snapend

@snap[east span-50]
<br>
<img src="assets/img/geo_forms.png" width="85%">

Salida de [r.geomorphon](https://grass.osgeo.org/grass-stable/manuals/r.geomorphon.html)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Análisis de paisaje
@snapend

@snap[west span-50]
<img src="assets/img/Raster_landscape.png">
@snapend

@snap[east span-50]
@size[20px](... varios add-ons para análisis de parches)

<img src="assets/img/Raster_r_pi_addons.png">
@snapend

@snap[south span-80]
<img src="assets/img/r_pi_searchtime.png" width="45%">

@size[18px](<a href="https://doi.org/10.1111/2041-210X.12827">Wegman et al., 2017</a>)
@snapend


---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos satelitales 

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Menú Imagery
@snapend

@snap[west span-50]
<img src="assets/img/Imagery_menu.png">
@snapend

@snap[east span-50]
<img src="assets/img/Imaging-Spectroscopy-Concept.png" width="90%">
@snapend

@snap[south span-100]
[Image processing](https://grass.osgeo.org/grass-stable/manuals/imageryintro.html) manual
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Administrar colores y visualización
@snapend

@snap[west span-50]
<br>
<img src="assets/img/Imagery_colors.png">
<br><br><br>
@snapend

@snap[east span-50]
<br>
<img src="assets/img/i_colors_enhance.jpg" width="85%">
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Transformaciones
@snapend

@snap[west span-50]
<img src="assets/img/Imagery_transform.png">
@snapend

@snap[east span-50]
@ul[header-footer-list-shrink](false)
- [i.pca](https://grass.osgeo.org/grass-stable/manuals/i.pca.html): Análisis de componentes principales
- [i.fft](https://grass.osgeo.org/grass-stable/manuals/i.fft.html): Transformada rápida de Fourier 
- [i.pansharpen](https://grass.osgeo.org/grass-stable/manuals/i.pansharpen.html): Algoritmos de fusión de imágenes para mejorar la resolución de los canales multiespectrales con una banda pancromática de alta resolución
@ulend
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Clasificación
@snapend

@snap[west span-50]
<img src="assets/img/Imagery_classification.png">
@snapend

@snap[east span-50]
<br>
@ul[header-footer-list-shrink](false)
- [r.learn.ml](https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html): Clasificación supervisada y regresión con Machine Learning
- [r.fuzzy.system](https://grass.osgeo.org/grass7/manuals/addons/r.fuzzy.system.html): Sistema de clasificación autónomo de lógica difusa
- [i.ann.maskrcnn](https://grass.osgeo.org/grass7/manuals/addons/i.ann.maskrcnn.html): Clasificación supervisada con redes neuronales artificiales
- [i.object.activelearning](https://grass.osgeo.org/grass7/manuals/addons/r.object.activelearning.html): Aprendizaje activo para la clasificación de objetos raster
@ulend
@snapend

@snap[south span-100]
@size[20px](Métodos supervisados y no supervisados, ver la wiki de <a href="https://grasswiki.osgeo.org/wiki/Image_classification">Image classification</a> para más detalles)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Segmentación y OBIA
@snapend

@snap[west span-40 text-06]
<br><br>
[Clasificación basada en objetos](https://github.com/tgrippa/Opensource_OBIA_processing_chain)
<img src="assets/img/remotesensing_grass_obia.png" width="85%">
@snapend

@snap[east span-60]
<br>
@ul[header-footer-list-shrink](false)
- [i.segment](https://grass.osgeo.org/grass77/manuals/i.segment.html): Identifica segmentos (objetos) a partir de datos de imágenes
- [i.segment.hierarchical](https://grass.osgeo.org/grass7/manuals/addons/i.segment.hierarchical.html): Segmentación jerárquica
- [i.segment.stats](https://grass.osgeo.org/grass7/manuals/addons/i.segment.stats.html): Calcula estadísticas para describir segmentos u objetos
- [i.segment.uspo](https://grass.osgeo.org/grass7/manuals/addons/i.segment.uspo.html): Optimización no supervisada de parámetros de segmentación
- [i.superpixels.slic](https://grass.osgeo.org/grass7/manuals/addons/i.superpixels.slic.html): Realiza la segmentación de imágenes mediante el método SLIC
@ulend
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Herramientas genéricas y para sensores específicos
@snapend

@snap[west span-60]
<br>
<img src="assets/img/Imagery_satellite_especif_tools.png">
<br>
@snapend

@snap[east span-40]
<br>
<img src="assets/img/i_atcorr_B02_atcorr.png" width="85%">
<br>
@size[12px](Banda 2 de Sentinel-2A después de *i.atcorr*)
@snapend

@snap[south span-100]
@size[20px](... además de diversas extensiones para MODIS, Sentinel 2, Landsat, SRTM, GPM, etc.)
<br><br>
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Productos derivados de sensado remoto
@snapend

@snap[west span-40]
<br>
<img src="assets/img/Imagery_products.png">
<br><br>
<img src="assets/img/ndvi.png" width="80%">
@snapend

@snap[east span-60]
<br><br>
@ul[header-footer-list-shrink](false)
- [i.wi](https://grass.osgeo.org/grass7/manuals/addons/i.wi.html): Calcula diferentes tipos de índices de agua
- [i.lswt](https://grass.osgeo.org/grass7/manuals/addons/i.lswt.html): Calcula la temperatura de la superficie de los lagos a partir de la temperatura de brillo
- [i.landsat8.swlst](https://grass.osgeo.org/grass7/manuals/addons/i.landsat8.swlst.html): Algoritmo split-window para estimar LST a partir de datos Landsat 8 OLI/TIRS
- [i.rh](https://grass.osgeo.org/grass7/manuals/addons/i.rh.html): humedad relativa, vapor de agua
- [i.water](https://grass.osgeo.org/grass7/manuals/addons/i.water.html): Detección de agua a partir de índices derivados de datos satelitales
@ulend
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos raster 3D

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Menú raster 3D
@snapend

@snap[west span-50]
<br>
<img src="assets/img/3D_raster_menu.png">
<br><br>
@snapend

@snap[east span-50]
<br>
<img src="assets/img/raster3d_layout.png" width="80%">
<br>
@size[18px](Sistema de coordenadas raster 3D y su disposición interna)
@snapend

@snap[south span-100]
[3D raster processing](https://grass.osgeo.org/grass-stable/manuals/raster3dintro.html) manual
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos vectoriales

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Menú Vector
@snapend

@snap[west span-50]
<br>
<img src="assets/img/Vector_menu.png">
@snapend

@snap[east span-50]
<br>
<img src="assets/img/vector_types.png">

@size[18px](Formatos vectoriales topológicos en GRASS GIS)
@snapend

@snap[south span-100]
[Vector processing](https://grass.osgeo.org/grass-stable/manuals/vectorintro.html) manual
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Mantenimiento de la topología
@snapend

@snap[midpoint span-90]
<img src="assets/img/Vector_topology_maint.png" width="80%">
<br><br><br><br>
@snapend

@snap[south-west span-65]
<img src="assets/img/v_clean.png" width="60%"><br>
@size[16px](Limpieza de errores de topología en mapa vectorial)
@snapend

@snap[south-east span-65]
<img src="assets/img/v_generalize_smooth.png" width="60%"><br>
@size[16px](Suavizado. Ver también la wiki de <a href="https://grasswiki.osgeo.org/wiki/V.generalize_tutorial">v.generalize</a>)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Selección y superposición
@snapend

@snap[west span-50]
<img src="assets/img/Vector_select.png">
<br><br><br><br>
<img src="assets/img/Vector_overlay.png">
@snapend

@snap[east span-50]
<br>
<img src="assets/img/v_select_op_touches.png" width="60%">
@size[15px](Operador *TOUCHES*)
<br><br><br>
<img src="assets/img/v_overlay_op_not.png" width="50%">
@size[15px](Operador *NOT*)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Análisis de redes
@snapend

@snap[west span-60]
<img src="assets/img/Vector_network_analysis.png">
@snapend

@snap[east span-40]
<img src="assets/img/v_net_distance.png">
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Reportes, estadísticas y actualización de atributos
@snapend

@snap[west span-50]
<br>
<img src="assets/img/Vector_report_stats.png">

<img src="assets/img/v_univar.png">
@snapend

@snap[east span-50]
<br><br>
<img src="assets/img/Vector_update_attr.png">

<img src="assets/img/v_rast_stats.png" width="90%">
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## @fa[database] Manejo de bases de datos

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Menú Database
@snapend

@snap[west span-50]
<img src="assets/img/DB_menu.png" width="95%">
<br><br><br><br>
@snapend

@snap[east span-50]
<img src="assets/img/vector_db_connections.png">
<br><br><br>
@snapend

@snap[south span-60]
<img src="assets/img/db_execute.png" width="60%">
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## @fa[layer-group] Procesamiento de datos temporales

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Menú Temporal 
@snapend

@snap[west span-50]
<img src="assets/img/Temporal_menu.png">
@snapend

@snap[east span-50 text-left]
@ul[header-footer-list-shrink](false)
- importar/exportar
- topología temporal
- agregación
- acumulación
- álgebra temporal
- interpolación
@ulend
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Modelador gráfico

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Modelo gráfico y su traducción a Python
@snapend

@snap[west span-50]
<img src="assets/img/graphical_modeller.png">
@snapend 

@snap[east span-50]
<img src="assets/img/graphical_modeller_python.png" width="90%">
@snapend

@snap[south span-100]
Ver el manual de [g.gui.gmodeler](https://grass.osgeo.org/grass-stable/manuals/wxGUI.gmodeler.html) para más detalles.
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Herramientas de visualización

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Map display: pestaña *Consola*
@snapend

@snap[south span-100]
<img src="assets/img/map_display_and_gui_console.png" width="85%">
<br>
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Map display: pestaña *Datos*
@snapend

@snap[south span-100]
<img src="assets/img/map_display_and_data_tab.png" width="85%">
<br>
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Map display: vista 3D
@snapend

@snap[south span-100]
<img src="assets/img/3d_view.png" width="90%">
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Monitores wx
@snapend

@snap[west span-40]
En la terminal:
<img src="assets/img/terminal_wx0_call.png">
@snapend

@snap[east span-60]
<br>
<img src="assets/img/wx_monitor.png" width="90%">
@snapend

@snap[south span-100]
@size[26px](Los monitores wx tienen los mismos **botones** que el Map Display en la GUI)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Map-swipe
@snapend

@snap[midpoint span-100]
<br>
<img src="assets/img/map_swipe.png" width="60%">
@snapend

@snap[south span-100]
[g.gui.mapswipe](https://grass.osgeo.org/grass-stable/manuals/g.gui.mapswipe.html)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Herramienta de animación
@snapend

@snap[midpoint span-100]
<br>
<img src="assets/img/lsat5_animation.gif" width="70%">
@snapend

@snap[south span-100]
[g.gui.animation](https://grass.osgeo.org/grass-stable/manuals/g.gui.animation.html)
@snapend

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Compositor cartográfico
@snapend

@snap[midpoint span-100]
<br>
<img src="assets/img/cartographic_comp_draft.png" width="50%">
@snapend

@snap[south span-100]
[g.gui.psmap](https://grass.osgeo.org/grass77/manuals/g.gui.psmap.html)
@snapend

+++

Exportar como .ps .eps or .pdf

<img src="assets/img/elevation.png" width="60%">

+++

Archivo .psmap para automatizar la composición cartográfica

@code[bash](code/elevation.psmap)

@[19](Mapa raster)
@[21-29](Vector de áreas)
@[30-40](Vector de líneas)
@[41-49](Paleta de colores raster para la leyenda)
@[50-57](Leyenda para el mapa vectorial)
@[58-67](Barra de escala)

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## @fa[plug] Add-ons @fa[plug]

<br>

+++?color=linear-gradient(180deg, #8EA33B 15%, white 15%)

@snap[north text-white span-100]
Algunos (otros) add-ons
@snapend

@snap[midpoint span-100]
<br>
@ul[header-footer-list-shrink](false)
- [i.modis](https://grass.osgeo.org/grass-stable/manuals/addons/i.modis.html): Herramientas para la descarga y el procesamiento de productos MODIS utilizando pyModis
- [i.sentinel](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.html): Herramientas para la descarga y el procesamiento de los productos de Copernicus Sentinel
- [i.landsat](https://grass.osgeo.org/grass-stable/manuals/addons/i.landsat.html): Herramientas para la descarga y el procesamiento de datos Landsat
- [r.hants](https://grass.osgeo.org/grass-stable/manuals/addons/r.hants.html): Aproxima una serie temporal periódica con armónicos
- [r.bioclim](https://grass.osgeo.org/grass-stable/manuals/addons/r.bioclim.html): Calcula índices bioclimáticos
- ... y **otros 300+** en el [repo oficial](https://github.com/OSGeo/grass-addons)!!
@ulend
@snapend

@snap[south span-100]
@fa[glasses text-green] @size[24px](<a href="https://grass.osgeo.org/grass7/manuals/addons/">https://grass.osgeo.org/grass7/manuals/addons/</a>) @fa[glasses text-green]
@snapend

--- 

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Ejercicio: Familiarizándonos con GRASS GIS](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=exercises/01_getting_familiar&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
