---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos satelitales en GRASS GIS

<br>

---

@snap[north-west span-60]
<h3>Contenidos</h3>
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Nociones básicas sobre datos satelitales en GRASS GIS
- Búsqueda y descarga de datos Landsat
- Calibración y corrección atmosférica: de número digital a reflectancia
- Ajuste de colores y composiciones RGB
- Máscara de nubes a partir de banda de calidad
- Fusión de datos/Pansharpening
- Índices espectrales de agua y vegetación
- Clasificación no supervisada
@olend
@snapend

---

### Nociones básicas sobre datos satelitales en GRASS GIS

Los datos satelitales en general vienen en formato raster @fa[arrow-right text-green] aplican las mismas reglas
<br>

> Los comandos @color[#8EA33B](*i.**) se orientan explícitamente al procesamiento de datos satelitales (aunque algunos puedan usarse para otros datos raster)

@size[24px](Para más detalles ver el manual <a href="https://grass.osgeo.org/grass-stable/manuals/imageryintro.html">Imagery Intro</a> y la wiki <a href="https://grasswiki.osgeo.org/wiki/Image_processing">Image Processing</a>)

---

@snap[north span-100]
### Datos
@snapend

@snap[west span-60]
Escenas Landsat 8 (OLI)
@ul[list-content-verbose](false)
- Fechas: 14/01/2020 y 02/03/2020
- Path/Row: 229/082
- CRS: UTM zona 20 N (EPSG:32620)
@ulend
<br><br>
@snapend

@snap[east span-50]
![L8](https://landsat.gsfc.nasa.gov/sites/landsat/files/2013/01/ldcm_2012_COL.png)
<br><br>
@snapend

@snap[south span-100 text-09]
@fa[download text-green] Descargar las escenas [L8 14/01/2020 (979Mb)](https://drive.google.com/file/d/1ytQp-xin1FQr_hqtDJRLgK6g4eXwK-WI/view?usp=sharing) y [L8 02/03/2020 (880Mb)](https://drive.google.com/file/d/1Gg8FbhwpIQR-GyYepM4uw_9IOjEnji_N/view?usp=sharing) y moverlas a `$HOME/gisdata/landsat_data`. **No descomprimir!**
<br>
@fa[download text-green] Descargar el [código](https://gitlab.com/veroandreo/maie-procesamiento/raw/master/code/04_L8_cba_imagery_code.sh?inline=false) para seguir esta sesión.
<br><br>
@snapend

+++

Historia de la mision Landsat

@img[span-85](assets/img/Landsat_history.jpg)

+++

![L8 vs L7 bands](https://landsat.gsfc.nasa.gov/sites/landsat/files/2013/01/ETM+vOLI-TIRS-web_Feb20131.jpg)

@size[24px](Comparación entre las bandas de Landsat 7 ETM+ y Landsat 8 OLI. Fuente: <https://landsat.gsfc.nasa.gov/landsat-data-continuity-mission/> y <a href="https://landsat.usgs.gov/what-are-band-designations-landsat-satellites">detalle de las bandas Landsat</a>)

---

Iniciar GRASS GIS, crear un nuevo mapset y establecer la región computacional

@code[bash](code/04_L8_cba_imagery_code.sh)

@[11-17](Iniciar GRASS GIS en *posgar2007_4_cba/PERMANENT*)
@[19-20](Corroborar la proyección)
@[22-23](Crear un nuevo mapset llamado *landsat8*)
@[25-26](Listar los mapsets accesibles)
@[28-29](Listar los mapas vectoriales disponibles)
@[31-34](Extraer el radio urbano de Córdoba)
@[36-38](Establecer la región computacional al radio urbano de Córdoba)
   
---

Descargar e importar los datos L8

@code[bash](code/04_L8_cba_imagery_code.sh)

@[41-47](Instalar la extensión *i.landsat*)
@[49-52](Buscar escenas de Landsat 8 disponibles)
@[54-57](Descargar las escenas seleccionadas)   
@[59-60](Imprimir las bandas dentro de la carpeta)
@[62-65](Imprimir sólo las bandas seleccionadas con un patrón)
@[67-71](Importar bandas, recortar y reproyectar al vuelo)
@[72-78](Listar bandas importadas y revisar metadatos)

+++

@snap[north span-100]
Opción *Directorio* para importar desde la GUI
@snapend

@snap[west span-50]
<br>
<img src="assets/img/import_directory_1.png">
@snapend

@snap[east span-50]
<br>
<img src="assets/img/import_directory_2.png">
@snapend

---

### Pre-procesamiento de datos satelitales

<br>
![Workflow](assets/img/rs_workflow.jpg)

---

### De número digital (ND) a reflectancia y temperatura

- Los datos L8 OLI vienen en 16-bit con rango de datos entre 0 y 65535.
- [i.landsat.toar](https://grass.osgeo.org/grass-stable/manuals/i.landsat.toar.html) convierte ND en reflectancia TOA (y temperatura de brillo) para todos los sensores Landsat. Opcionalmente proporciona reflectancia de superficie (BOA) después de la corrección DOS. 
- [i.atcorr](https://grass.osgeo.org/grass-stable/manuals/i.atcorr.html) proporciona un método de corrección atmosférica más complejo para gran variedad de sensores (S6).

+++

ND a Reflectancia y Temperatura de Superficie

@code[bash](code/04_L8_cba_imagery_code.sh)

@[80-91](Convertir DN a reflectancia superficial y temperatura - método DOS)
@[93-99](Corroborar info antes y después de la conversión para una banda)

+++

@img[span-80](assets/img/L8_band10_kelvin.png)

@size[24px](Banda 10 de L8 con la paleta de colores *kelvin*)

+++

> @fa[tasks] **Tarea**:
> 
> Ahora, sigan los mismos pasos para la escena del 02/03/2020. Qué notan de diferente?

---

Ajuste de color y composiciones RGB

@code[bash](code/04_L8_cba_imagery_code.sh)

@[101-111](Ajuste de colores para una composición RGB color natural)
@[113-118](Mostrar la combinación RGB - *d.rgb*)

+++

> @fa[tasks] **Tarea**: 
>
> Seguir los mismos pasos para una composición falso color 543. Sobre qué bandas debieran realizar el ajuste? 

+++

<img src="assets/img/L8_composite432.png" width="48%"> 
<img src="assets/img/L8_composite543.png" width="48%">

@size[24px](Composiciones color natural 432 y falso color 543)

---

### Enmascarado de nubes con banda QA

- Landsat 8 proporciona una banda de calidad (QA) con valores enteros de 16 bits que representan las combinaciones de superficie, atmósfera y condiciones del sensor que pueden afectar la utilidad general de un determinado pixel. 
- La extensión [i.landsat.qa](https://grass.osgeo.org/grass-stable/manuals/addons/i.landsat.qa.html) reclasifica la banda QA de Landsat 8 de acuerdo a la calidad del pixel. 

@size[24px](Más información sobre la banda QA de L8:<br>https://www.usgs.gov/core-science-systems/nli/landsat)

+++

Aplicar máscara de nubes

@code[bash](code/04_L8_cba_imagery_code.sh)
 
@[126-130](Crear las reglas para identificar las nubes y sombras de nubes)
@[132-136](Reclasificar la banda QA en función de las reglas)
@[138-139](Reportar el área cubierta por nubes)
@[141-147](Mostrar el mapa reclasificado)

+++

> @fa[tasks] **Tarea:** 
>
> Comparar visualmente la cobertura de nubes con la composición RGB 543.

+++

<img src="assets/img/L8_composite543.png" width="48%">
<img src="assets/img/L8_clouds.png" width="48%"> 

@size[24px](Composición falso color y máscara de nubes)

---

### Fusión de datos/Pansharpening
<br>
Vamos a usar la banda PAN (15 m) para mejorar la definición de las bandas espectrales de 30 m, por medio de:

> [i.fusion.hpf](https://grass.osgeo.org/grass-stable/manuals/addons/i.fusion.hpf.html), que aplica un método de adición basado en un filtro de paso alto

Otros métodos están implementados en [i.pansharpen](https://grass.osgeo.org/grass-stable/manuals/i.pansharpen.html)

+++

Fusión de datos/Pansharpening

@code[bash](code/04_L8_cba_imagery_code.sh)

@[150-156](Instalar la extensión *i.fusion.hpf*)
@[158-159](Cambiar la región a la banda PAN)
@[161-167](Ejecutar la fusión)
@[169-170](Listar los mapas resultantes usando un patrón de búsqueda)
@[172-175](Visualizar las diferencias con la herramienta mapswipe)

+++            

<img src="assets/img/L8_mapswipe_hpf.png" width="72%">

@size[24px](Datos originales 30 m y datos fusionados 15 m)

---

### Índices de agua y vegetación

@code[bash](code/04_L8_cba_imagery_code.sh)

@[178-184](Establecer la máscara de nubes para evitar el cómputo sobre las nubes)
@[186-192](Calcular el NDVI y establecer la paleta de colores)
@[194-199](Calcular NDWI y establecer la paleta de colores)
@[201-206](Mostrar los mapas)

+++

<img src="assets/img/L8_ndvi.png" width="49%">
<img src="assets/img/L8_ndwi.png" width="49%"> 

@size[24px](NDVI y NDWI a partir de datos Landsat 8)

+++

> @fa[tasks] **Tarea:** 
>
> Estimar NDVI y NDWI para la otra escena usando el módulo [i.vi](https://grass.osgeo.org/grass-stable/manuals/i.vi.html) 

---

### Clasificación No Supervisada

@ol[]
- Agrupar las bandas (i.e., hacer un stack): [i.group](https://grass.osgeo.org/grass-stable/manuals/i.group.html)
- Generar firmas para *n* número de clases: [i.cluster](https://grass.osgeo.org/grass-stable/manuals/i.cluster.html)
- Clasificar: [i.maxlik](https://grass.osgeo.org/grass-stable/manuals/i.maxlik.html)
@olend   
        
+++

Clasificación No Supervisada

@code[bash](code/04_L8_cba_imagery_code.sh)

@[209-215](Listar los mapas usando un patrón)
@[217-219](Crear un grupo de imágenes o *stack*)
@[221-225](Obtener estadísticos -firmas- para las *n* clases de interés con una muestra de pixeles)
@[227-231](Realizar la clasificación no supervisada de toda la imagen)
@[233-235](Mostrar el mapa clasificado)

+++

@img[span-60](assets/img/L8_unsup_class.png)

@size[24px](Clasificación No Supervisada)

+++

Información derivada adicional podría obtenerse con los siguientes módulos, entre otros:

- medidas de textura: [r.texture](https://grass.osgeo.org/grass-stable/manuals/r.texture.html), 
- medidas de diversidad: [r.diversity](https://grass.osgeo.org/grass-stable/manuals/addons/r.diversity.html), 
- estadísticas locales con información de contexto: [r.neighbors](https://grass.osgeo.org/grass-stable/manuals/r.neighbors.html),
- transformación tasseled cap: [i.tasscap](https://grass.osgeo.org/grass-stable/manuals/i.tasscap.html),
- etc.

---

### @fa[bookmark text-green] Clasificación en GRASS GIS @fa[bookmark text-green]

- [Topic classification](http://grass.osgeo.org/grass-stable/manuals/topic_classification.html) en los manuales de GRASS GIS
- [Image classification](http://grasswiki.osgeo.org/wiki/Image_classification) en la wiki
- [Ejemplos de clasificación](http://training.gismentors.eu/grass-gis-irsae-winter-course-2018/units/28.html) en el curso dictado en Noruega en 2018
- [Clasificación con Random Forest](https://neteler.gitlab.io/grass-gis-analysis/03_grass-gis_ecad_randomforest/) en la presentación del OpenGeoHub Summer School 2018 en Praga
- [Detección de cambios con Landsat](https://veroandreo.gitlab.io/post/jan2021_ilandsat_tutorial/)

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Ejercicio: Trabajamos con imágenes Sentinel 2](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/03_exercise_processing_s2.pdf)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
