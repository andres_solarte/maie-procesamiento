---?image=assets/img/grass.png&position=bottom&size=100% 30%

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de series de tiempo en GRASS GIS

<br>

---

@snap[north-west span-60]
<h3>Contenidos</h3>
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Nociones básicas
- TGRASS framework
- Creación de series de tiempo
- Álgebra temporal y variables temporales
- Distintos tipos de agregación
- Estadística zonal e islas de calor urbanas
- Conexión con R
@olend
@snapend
---

@color[#8EA33B](**GRASS GIS**) es **el primer SIG de código abierto** que incorporó
capacidades para **gestionar, analizar, procesar y visualizar datos espacio-temporales**,
así como las relaciones temporales entre series de tiempo.
<br><br>

@fa[layer-group fa-3x text-green]

+++

## TGRASS: GRASS Temporal

@ul
- Completamente @color[#8EA33B](basado en metadatos), por lo que no hay duplicación de datos
- Sigue una aproximación @color[#8EA33B](*Snapshot*), i.e., añade marcas de tiempo o *timestamps* a los mapas
- Una colección de mapas de la misma variable con timestamps se llama @color[#8EA33B](space-time dataset o STDS)
@ulend

+++

## TGRASS: GRASS Temporal

@ul
- Los mapas en una STDS pueden tener diferentes extensiones espaciales y temporales
- TGRASS utiliza una base de datos SQL para almacenar la extensión temporal y espacial de las STDS, así como las relaciones topológicas entre los mapas y entre las STDS en cada mapset.
@ulend

+++

## @fa[layer-group text-green] Space-time datasets @fa[layer-group text-green]

- Space time raster datasets (**ST@color[#8EA33B](R)DS**)
- Space time 3D raster datasets (**ST@color[#8EA33B](R3)DS**)
- Space time vector datasets (**ST@color[#8EA33B](V)DS**)

<br>

@fa[bullhorn text-orange fa-2x] Suppot for [**image collections**](https://github.com/OSGeo/grass/pull/63) is on the way!
 
+++

## Otras nociones básicas en TGRASS

@ul
- El tiempo puede definirse como @color[#8EA33B](intervalos) (inicio y fin) o como @color[#8EA33B](instancias) (sólo inicio)
- El tiempo puede ser @color[#8EA33B](absoluto) (por ejemplo, 2017-04-06 22:39:49) o @color[#8EA33B](relativo) (por ejemplo, 4 años, 90 días)
- @color[#8EA33B](Granularidad) es el mayor divisor común de todas las extensiones temporales (y posibles gaps) de los mapas de un STDS
@ulend

+++

### Otras nociones básicas en TGRASS

- @color[#8EA33B](Topología) se refiere a las relaciones temporales entre los intervalos de tiempo en una STDS

<img src="assets/img/temp_relation.png">

+++

### Otras nociones básicas en TGRASS

- @color[#8EA33B](Muestreo temporal) se utiliza para determinar el estado de un proceso durante un segundo proceso.

<img src="assets/img/temp_samplings.png" width="55%">

+++

## @fa[tools text-green] Módulos temporales @fa[tools text-green]

- @color[#8EA33B](**t.\***): Módulos generales para manejar STDS de todos los tipos
- @color[#8EA33B](**t.rast.\***): Módulos que tratan con STRDS
- @color[#8EA33B](**t.rast3d.\***): Módulos que tratan con STR3DS
- @color[#8EA33B](**t.vect.\***): Módulos que tratan con STVDS

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## TGRASS: marco general y flujo de trabajo

<br>

+++?image=assets/img/tgrass_flowchart.png&position=center&size=auto 93%

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Manos a la obra con series de tiempo raster en GRASS GIS

<br>

---

@snap[north span-100]
### Datos para la sesión
@snapend

@snap[west span-40]
<br>
@ul[](false)
- Producto MODIS: <a href="https://lpdaac.usgs.gov/products/mod11b3v006/">MOD11B3 Collection 6</a>
- Tile: h12v12
- Composiciones mensuales 
- Resolución espacial: 5600m
- Mapset *`modis_lst`* 
@ulend
@snapend

@snap[east span-60]
<br>
@img[](assets/img/mod11b3_h12v12.png)
@snapend

+++

### @fa[download text-green] Código para la sesión @fa[download text-green]

<br>
- [Comandos GRASS](https://gitlab.com/veroandreo/maie-procesamiento/raw/master/code/05_temporal_code.sh?inline=false)
- [Comandos R](https://gitlab.com/veroandreo/maie-procesamiento/raw/master/code/05_temporal_code.r?inline=false)
<br>

Iniciar GRASS GIS directamente en *`modis_lst`*
<br>

```bash
grass78 $HOME/grassdata/posgar2007_4_cba/modis_lst --gui
```

---

Establecer región computacional y máscara

@code[bash](code/05_temporal_code.sh) 

@[35-39](Listar los mapas raster y obtener información de uno de ellos)
@[43-61](Establecer la región computacional)
@[62-67](Aplicar máscara)

---

### Crear un conjunto de datos espacio-temporales (STDS)

**[t.create](https://grass.osgeo.org/grass-stable/manuals/t.create.html)**
<br>
- Crea una tabla SQLite en la base de datos temporal 
- Permite manejar grandes cantidades de mapas usando el STDS como entrada
- Necesitamos especificar:
  - *tipo de mapas* (raster, raster3d o vector)
  - *tipo de tiempo* (absoluto o relativo)

+++?code=code/05_temporal_code.sh&lang=bash&title=Crear una serie de tiempo raster (STRDS)

@[70-75](Crear la STRDS)
@[76-77](Chequear si la STRDS fue creada)
@[79-80](Obtener información sobre la STRDS)

---  

### Registrar mapas en una STDS

**[t.register](https://grass.osgeo.org/grass-stable/manuals/t.register.html)**
<br>
- Asigna o agrega timestamps a los mapas
- Necesitamos: 
  - el *STDS vacío* como entrada, i.e., la tabla SQLite contenedora, 
  - la *lista de mapas* que se registrarán, 
  - la *fecha de inicio*,
  - la opción de *incremento* junto con *-i* para la creación de intervalos 

+++?code=code/05_temporal_code.sh&lang=bash&title=Registrar mapas en STRDS (asignar *timestamps*)

@[84-89](Añadir timestamps a los mapas, i.e., registrar mapas - *nix)
@[90-93](Añadir timestamps a los mapas, i.e., registrar mapas - windows)
@[95-96](Chequear la información sobre la STRDS nuevamente)
@[98-99](Obtener la lista de mapas en la STRDS)
@[101-102](Chequear los valores mínimos y máximos de cada mapa)

@size[20px](Para más opciones, ver el manual de <a href="https://grass.osgeo.org/grass-stable/manuals/t.register.html">t.register</a> y la wiki sobre <a href="https://grasswiki.osgeo.org/wiki/Temporal_data_processing/maps_registration">opciones para registrar mapas en STDS</a>.)

+++
Representación gráfica de STDS
@code[bash](code/05_temporal_code.sh)

@[105-106](Crear una representación gráfica de la serie de tiempo)

+++

@img[width=700px](assets/img/g_gui_timeline_monthly.png)

@size[24px](Ver el manual de <a href="https://grass.osgeo.org/grass-stable/manuals/g.gui.timeline.html">g.gui.timeline</a>)

---

@snap[north span-100]
### Operaciones con álgebra temporal
**[t.rast.algebra](https://grass.osgeo.org/grass-stable/manuals/t.rast.algebra.html)**
@snapend

@snap[south list-content-verbose span-100]
@ul[](false)
- Realiza una amplia gama de operaciones de álgebra temporal y espacial basadas en la topología temporal de los mapas
@ulend
@ul[](false)
  - Operadores temporales: unión, intersección, etc.
  - Funciones temporales: *start_time()*, *start_doy()*, etc.
  - Operadores espaciales (subconjunto de [r.mapcalc](https://grass.osgeo.org/grass-stable/manuals/r.mapcalc.html))
  - Modificador de vecindario temporal: *[x,y,t]*
  - Otras funciones temporales como *t_snap()*, *buff_t()* o *t_shift()*
@ulend
<br>
@fa[rocket text-orange] **@size[30px](¡pueden combinarse en expresiones complejas!)** @fa[rocket text-orange]
@snapend

+++?code=code/05_temporal_code.sh&lang=bash&title=Desde K*50 a Celsius usando la calculadora temporal

@[110-114](Re-escalar a grados Celsius)
@[115-116](Ver info de la nueva serie de tiempo)

+++

Gráfico temporal: LST vs tiempo

@code[bash](code/05_temporal_code.sh)
@[120-126](Gráfico temporal de LST para la ciudad de Córdoba, Argentina)

@size[20px](Para un único punto, ver <a href="https://grass.osgeo.org/grass-stable/manuals/g.gui.tplot.html">g.gui.tplot</a>. Para un vector de puntos, ver <a href="https://grass.osgeo.org/grass-stable/manuals/t.rast.what.html">t.rast.what</a>.)

+++

@img[width=600px](assets/img/g_gui_tplot_final.png)

@size[24px](Las coordenadas del punto pueden ser escritas directamente, copiadas desde el mapa o seleccionadas interactivamente.)

---

## Listas y selecciones

- **[t.list](https://grass.osgeo.org/grass-stable/manuals/t.list.html)** para listar las STDS y los mapas registrados en la base de datos temporal,
- **[t.rast.list](https://grass.osgeo.org/grass-stable/manuals/t.rast.list.html)** para mapas en series temporales de rasters, y
- **[t.vect.list](https://grass.osgeo.org/grass-stable/manuals/t.vect.list.html)** para mapas en series temporales de vectores.

+++

@snap[north span-100]
### Variables usadas para hacer las listas y selecciones
@snapend

@snap[west span-45 text-08]
<br>
@box[rounded box-green](STRDS:#*id, name, creator, mapset, temporal_type, creation_time, start_time, end_time, north, south, west, east, nsres, ewres, cols, rows, number_of_cells, min, max*)
@snapend

@snap[east span-45 text-08]
<br>
@box[rounded box-green](STVDS:#*id, name, layer, creator, mapset, temporal_type, creation_time, start_time, end_time, north, south, west, east, points, lines, boundaries, centroids, faces, kernels, primitives, nodes, areas, islands, holes, volumes*)
@snapend

+++?code=code/05_temporal_code.sh&lang=bash&title=Ejemplos de listas y selecciones

@[130-132](Mapas cuyo valor mínimo es menor o igual a 10)
@[134-136](Mapas cuyo valor máximo es mayor a 30)
@[138-140](Mapas contenidos entre dos fechas)
@[142-144](Todos los mapas correspondientes al mes de Enero)

---

### Estadística descriptiva de STRDS

@code[bash](code/05_temporal_code.sh)

@[149-150](Imprimir estadísticas descriptivas univariadas para cada mapa dentro de la STRDS)
@[152-153](Obtener estadísticas extendidas con la opción -e)
@[155-157](Escribir la salida a un archivo de texto)

---

### Agregación temporal 1: Serie completa

**[t.rast.series](https://grass.osgeo.org/grass-stable/manuals/t.rast.series.html)**
<br>
- Agrega STRDS *completas* o partes de ellas usando la opción *where*.
- Diferentes métodos disponibles: promedio, mínimo, máximo, mediana, moda, etc.

+++

LST máxima y mínima del período 2015-2019

@code[bash](code/05_temporal_code.sh)

@[162-164](Obtener el mapa de la máxima LST del período)
@[166-168](Obtener el mapa de la mínima LST del período)
@[170-171](Cambiar la paleta de colores a *celsius*)

+++

> @fa[tasks] Comparar mapas con la herramienta Mapswipe

+++

@img[width=700px](assets/img/g_gui_mapswipe_lstmin.png)

---

### Operaciones usando variables temporales

**[t.rast.mapcalc](https://grass.osgeo.org/grass-stable/manuals/t.rast.mapcalc.html)**
<br>
- Ejecuta expresiones espacio-temporales tipo *r.mapcalc*
- Permite *operadores espaciales y temporales*, así como *variables internas* en la expresión
- Las variables temporales incluyen: *start_time(), end_time(), start_month(), start_doy()*, etc. 

+++

Cuál es el mes de máxima LST?

@code[bash](code/05_temporal_code.sh)

@[176-180](Obtener el mes en que ocurre el máximo de LST en cada pixel)
@[182-183](Obtener información del mapa resultante)
@[185-188](Obtener el primer mes en que aparece el máximo de LST)
@[190-192](Remover la STRDS intermedia y los mapas que contiene: month_max_lst)

+++?code=code/05_temporal_code.sh&lang=bash&title=Mostrar el mapa resultante desde la terminal

@[197-198](Abrir un monitor wx)
@[200-201](Mostrar el mapa raster)
@[203-204](Mostrar sólo los bordes del mapa vectorial de NC)
@[206-208](Agregar leyenda)
@[210-211](Agregar barra de escala)
@[213-214](Agregar Norte)
@[216-218](Agregar título)

+++

@img[width=800px](assets/img/month_max_lst.png)

+++

Podríamos haber hecho lo mismo pero anualmente para conocer en qué mes ocurre el máximo en cada año y así evaluar la ocurrencia de tendencias. Cómo lo harían?

---

### Agregación temporal 2: granularidad

**[t.rast.aggregate](https://grass.osgeo.org/grass-stable/manuals/t.rast.aggregate.html)**
<br>
- Agrega mapas raster dentro de STRDS con diferentes **granularidades** 
- La opción *where* permite establecer fechas específicas para la agregación
- Diferentes métodos disponibles: promedio, mínimo, máximo, mediana, moda, etc.

+++?code=code/05_temporal_code.sh&lang=bash&title=De LST mensual a estacional

@[223-227](LST media estacional)
@[229-230](Chequear info)
@[232-233](Chequear lista de mapas)

+++

> @fa[tasks] **Tarea** 
> 
> Comparar las líneas de tiempo mensual y estacional con [g.gui.timeline](https://grass.osgeo.org/grass-stable/manuals/g.gui.timeline.html)

```bash
g.gui.timeline inputs=LST_Day_monthly_celsius,LST_Day_mean_3month
```

+++?code=code/05_temporal_code.sh&lang=bash&title=Graficar LST estacional con monitores wx

@[238-239](Establecer la paleta de colores *celsius* para la STRDS estacional)
@[241-242](Iniciar un monitor *Cairo*)
@[244-248](Crear el primer frame)
@[250-254](Crear el segundo frame)
@[256-260](Crear el tercer frame)
@[262-266](Crear el cuarto frame)
@[268-269](Liberar el monitor)

+++

@img[width=1050px](assets/img/frames.png)

@size[24px](LST estacional en 2015)

+++

> @fa[tasks] **Tarea**
>
> Ahora que ya conocen [t.rast.aggregate](https://grass.osgeo.org/grass-stable/manuals/t.rast.aggregate.html), 
> extraigan el mes de máximo LST por año y luego vean si hay alguna tendencia positiva o negativa,
> es decir, si los valores máximos de LST se observan más tarde o más temprano con el tiempo (años)

+++

Una solución podría ser...
<br>

```bash zoom-13
t.rast.aggregate \
  input=LST_Day_monthly_celsius \
  output=month_max_LST_per_year \
  basename=month_max_LST suffix=gran \
  method=max_raster \
  granularity="1 year" 

t.rast.series \
  input=month_max_LST_per_year \
  output=slope_month_max_LST \
  method=slope
```

---

### Animaciones

@code[bash](code/05_temporal_code.sh)

@[274-275](Animación de la serie estacional de LST)

@size[20px](Ver el manual de <a href="https://grass.osgeo.org/grass-stable/manuals/g.gui.animation.html">g.gui.animation</a> para más opciones y ajustes.)

---

@snap[north span-100]
### Agregación vs Climatología
@snapend

@snap[west span-45]
<img src="assets/img/aggregation.png">
<br>
Agregación por granularidad
@snapend

@snap[east span-50]
<img src="assets/img/climatology.png">
<br>
Agregación tipo climatología
@snapend

+++

### Climatologías mensuales
@code[bash](code/05_temporal_code.sh)

@[280-284](LST promedio de Enero)
@[286-291](Climatología para todos los meses - *nix)
@[293-298](Climatología para todos los meses - windows)

+++

> @fa[tasks] **Tarea**
> 
> - Comparar las medias mensuales con las climatologías mensuales
> - Las climatologías que creamos forman una STRDS?

---

### Anomalías anuales
<br>

> `\[AnomaliaStd_i = \frac{Media_i - Media}{SD}\]`

<br>
Se necesitan:

- promedio y desviación estándar general de la serie
- promedios anuales

+++

### Anomalías anuales

@code[bash](code/05_temporal_code.sh)

@[303-305](Obtener el promedio general de la serie)
@[307-309](Obtener el desvío estándar general de la serie)
@[311-314](Obtener los promedios anuales)
@[316-318](Estimar las anomalías anuales)
@[320-321](Establecer la paleta de colores *differences*)
@[323-324](Animación)

---

@snap[north span-100]
### Isla de calor superficial urbana
(Surface Urban Heat Island - SUHI)
@snapend

@snap[west span-60 text-08]
<br><br>
@ul[](false)
- La temperatura del aire de una zona urbana es más alta que la de las zonas cercanas
- La UHI tiene efectos negativos en la calidad del agua y el aire, la biodiversidad, la salud humana y el clima.
- La SUHI también está muy relacionada con la salud, ya que influye en la UHI 
@ulend
@snapend

@snap[east span-40]
<br><br>
@img[span-90](https://res.mdpi.com/remotesensing/remotesensing-11-01212/article_deploy/html/images/remotesensing-11-01212-g002-550.jpg)
<br>
@size[14px](SUHI y área rural en Buenos Aires. Fuente: <a href="https://www.mdpi.com/2072-4292/11/10/1212/htm">Wu et al, 2019.</a>)
@snapend

+++

### Estadística zonal en series de tiempo de datos raster

**[v.strds.stats](https://grass.osgeo.org/grass7/manuals/addons/v.strds.stats.html)**
<br>
- Permite obtener datos de series de tiempo agregados espacialmente para polígonos de un mapa vectorial


+++

### SUHI estival para el *Gran Córdoba* y alrededores

@code[bash](code/05_temporal_code.sh)

@[329-330](Instalar la extensión *v.strds.stats*)
@[332-333](Listar mapas)
@[335-341](Extraer LST promedio de verano para el Gran Córdoba)
@[343-346](Crear buffer externo - 30 km)
@[348-351](Crear buffer interno - 15 km)
@[353-357](Remover el área del buffer 15 km del buffer de 30 km)

+++

@img[span-65](assets/img/suhi_buffers.png)

@size[24px](Límites del Gran Córdoba y el área rural circundante)


+++

### SUHI estival para el *Gran Córdoba* y alrededores

@code[bash](code/05_temporal_code.sh)

@[359-364](Extraer estadísticas para los alrededores del Gran Córdoba)
@[366-368](Chequear la LST estival promedio para el Gran Córdoba y alrededores)

+++

### GRASS y @fab[r-project text-blue] para hacer mapas

<br>
Vamos a usar **R** y **RStudio** para crear mapas con los vectores resultantes

<br>

En la terminal de GRASS GIS:
<br><br>
`rstudio &`

+++

### Haciendo mapas en R con datos de GRASS GIS

@code[r](code/05_temporal_code.r)

@[7-9](Cargar las librerías *rgrass7* y *sf*)
@[11-12](Listar los vectores disponibles en el mapset)
@[14-17](Leer e importar los mapas vectoriales desde GRASS GIS)
@[19-21](Remover columnas extra)
@[23-24](Pegar los dos vectores)
@[26-27](Gráfico rápido con *sf*)

+++

@img[span-100](assets/img/sf_plot.png)

+++

### Haciendo mapas en R con datos de GRASS GIS

@code[r](code/05_temporal_code.r)

@[30-33](Usando la librería *ggplot*)
@[35-44](Arreglar los datos desde formato *wide* a *long*)
@[46-47](Reemplazar valores en la columna *YEAR*)
@[49-55](Graficar)

+++

@img[span-70](assets/img/ggplot.png)

+++

### Haciendo mapas en R con datos de GRASS GIS

@code[r](code/05_temporal_code.r)

@[58-59](Usando la librería *tmap*)
@[61-64](Graficar)

+++

@img[span-70](assets/img/tmap.png)

+++

### Haciendo mapas en R con datos de GRASS GIS

@code[r](code/05_temporal_code.r)

@[67-69](Visualización rápida e interactiva con *mapview*)

+++

@img[span-45](assets/img/mapview1.png)
@img[span-45](assets/img/mapview2.png)

@size[24px](Visualización con *mapview* y diferentes mapa base)

---

<img src="assets/img/gummy-question.png" width="45%">

---

## @fa[bookmark text-green] Recursos (muy) útiles @fa[bookmark text-green]

- [Temporal data processing wiki](https://grasswiki.osgeo.org/wiki/Temporal_data_processing)
- [GRASS GIS and R for time series processing wiki](https://grasswiki.osgeo.org/wiki/Temporal_data_processing/GRASS_R_raster_time_series_processing)
- [GRASS GIS temporal workshop at NCSU](http://ncsu-geoforall-lab.github.io/grass-temporal-workshop/)
- [GRASS GIS workshop held in Jena 2018](http://training.gismentors.eu/grass-gis-workshop-jena-2018/index.html)
- [GRASS GIS course IRSAE 2018](http://training.gismentors.eu/grass-gis-irsae-winter-course-2018/index.html)
- [Space-time satellite data for disease ecology - OpenGeoHub Summer School 2019](https://www.youtube.com/watch?v=nu_ZFvmAFGw)

---

## @fa[book text-green] Referencias @fa[book text-green]

- Gebbert, S., Pebesma, E. (2014) *A temporal GIS for field based environmental modeling*. Environmental Modelling & Software, 53, 1-12. [DOI](https://doi.org/10.1016/j.envsoft.2013.11.001)
- Gebbert, S., Pebesma, E. (2017) *The GRASS GIS temporal framework*. IJGIS 31, 1273-1292. [DOI](http://dx.doi.org/10.1080/13658816.2017.1306862)
- Gebbert, S., Leppelt, T., Pebesma, E. (2019) *A Topology Based Spatio-Temporal Map Algebra for Big Data Analysis*. Data, 4, 86. [DOI](https://doi.org/10.3390/data4020086)

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Ejercicio: Manos a la obra con series temporales de NDVI](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=exercises/05_ndvi_time_series&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
