# Procesamiento digital de imágenes satelitales y SIG

Clases y ejercicios sobre Object Based Image Analysis (OBIA) y análisis de series de tiempo en [**GRASS GIS**](https://grass.osgeo.org/) y R para la asignatura **Procesamiento digital de imágenes satelitales y SIG**. [Maestría en Aplicaciones de la Información Espacial (MAIE)](https://ig.conae.unc.edu.ar/maestria-en-aplicaciones-de-informacion-espacial/). Instituto Gulich. UNC-CONAE. 

## Presentaciones y ejercicios

### Presentaciones:

- [Breve introducción a GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/01_intro.pdf)
- [Un paseo por las funciones de GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/02_intro_capabilities.pdf)
- [Procesamiento de datos raster en GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/03_raster.pdf)
- [Object based image analysis - OBIA](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/05_obia.pdf)
- [Procesamiento de series de tiempo en GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/06_time_series.pdf)
- [Análisis de series tiempo con BFAST](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/07_bfast.pdf)

### Ejercicios:

- [Familiarizándonos con GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/01_exercise_getting_familiar.pdf)
- [Crear un location e importar mapas a GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/02_exercise_create_location.pdf)
- [Clasificación basada en objetos con datos SPOT](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/04_exercise_obia.pdf)
- [Manos a la obra con series temporales de NDVI](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/05_exercise_ndvi_time_series.pdf)

### Presentaciones y ejercicios opcionales (pero recomendados):

- [Procesamiento de datos satelitales en GRASS GIS](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/04_imagery.pdf)
- [Trabajamos con imágenes Sentinel 2](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/03_exercise_processing_s2.pdf)

## Software

Para el desarrollo de este curso se utilizará **GRASS GIS 7.8.5**, que es
la versión estable actual de GRASS. Se la puede instalar a través de sus diversos 
binarios o ejecutables, o a través de OSGeo-Live que incluye todos los demás 
paquetes de software de [OSGeo](https://www.osgeo.org/). 
Se recomienda ver la presentación 
[**Guía de instalación**](https://gitlab.com/veroandreo/maie-procesamiento/-/tree/master/pdf/00_instalacion.pdf) 
para más detalles. No obstante, aquí abajo se muestran los links para
los sistemas operativos mas comúnmente usados.

:exclamation: Para que el curso se desarrolle de manera mas fluida, **se recomienda usar Linux** como sistema operativo :exclamation:

### Instaladores para los sistemas operativos mas comunes

##### Ubuntu Linux

GRASS GIS 7.8.5 se puede instalar usando el repositorio "unstable":

```
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
sudo apt-get install grass grass-dev grass-gui
```

##### Fedora, openSuSe y otras distros

Para otras distribuciones de Linux, incluyendo **Fedora** y **openSuSe**, 
simplemente instalar GRASS GIS a partir de los repositorios oficiales. 

```
sudo dnf install grass grass-gui grass-devel
```

Ver también el siguiente [enlace](https://grass.osgeo.org/download) para más detalles.

##### MS Windows

Existen dos opciones diferentes para los usuarios de MS Windows:
1. Ejecutable con todas las dependencias: [32-bit](https://grass.osgeo.org/grass78/binary/mswindows/native/x86/WinGRASS-7.8.3-1-Setup-x86.exe) | [64-bit](https://grass.osgeo.org/grass78/binary/mswindows/native/x86_64/WinGRASS-7.8.3-1-Setup-x86_64.exe) 
2. Paquete OSGeo4W: [32-bit](http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86.exe) | [64-bit](http://download.osgeo.org/osgeo4w/osgeo4w-setup-x86_64.exe) 

Se recomienda optar por la segunda opción, **OSGeo4W**, ya que permite
instalar todos los programas y paquetes de OSGeo. Al elegir esta opción
asegurarse de de seleccionar **GRASS GIS** y **msys**. Este último, permitirá 
usar algunos trucos disponibles normalmente en las terminales *bash*, tales
como ciclos, back ticks, autocompletado, historia de comandos, entre otros. 

##### OSGeo-live 

[OSGeo-Live](https://live.osgeo.org/) es un sistema booteable auto-contenido basado 
en Lubuntu, que puede cargarse en un USB o una máquina virtual, y que permite probar
una amplia variedad de software geo-espacial sin necesidad de instalar nada.
Existen diferentes opciones para ejecutar OSGeo-Live:

* [Ejecutar OSGeo-Live en una Máquina Virtual](https://live.osgeo.org/en/quickstart/virtualization_quickstart.html)
* [Ejecutar OSGeo-Live desde un USB](https://live.osgeo.org/en/quickstart/usb_quickstart.html)

Para una guía rápida, ver [aquí](https://live.osgeo.org/en/quickstart/osgeolive_quickstart.html).

### Otros paquetes de software y dependencias necesarias

Las siguientes son librerías de Python requeridas por las extensiones que usaremos durante el curso:
- [pyModis](http://www.pymodis.org): librería para trabajar con datos MODIS. Ofrece descarga, mosaico, re-proyección, conversión de formato y extracción de bandas de calidad. Esta librería es necesaria para *[i.modis](https://grass.osgeo.org/grass7/manuals/addons/i.modis.html)*.
- [sentinelsat](https://github.com/sentinelsat/sentinelsat): utilidad para búsqueda y descarga de imágenes Copernicus Sentinel del [Copernicus Open Access Hub](https://scihub.copernicus.eu/). Esta librería es necesaria para *[i.sentinel](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.html)*.
- [landsatxplore](https://github.com/yannforget/landsatxplore): utilidad para búsqueda y descarga de imágenes Landsat 5, 7 y 8 desde [Earth Explorer](http://earthexplorer.usgs.gov/). Esta librería es necesaria para *[i.landsat](https://grass.osgeo.org/grass7/manuals/addons/i.landsat.html)*.
- [scikit-learn](https://scikit-learn.org/stable/): librería de python para machine learning. Es requerida por la extensión *[r.learn.ml](https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html)*.
- [pandas](https://pypi.org/project/pandas/): librería de python para *data analysis*. Es requerida por *[i.sentinel](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.html)*.
- [matplotlib](https://matplotlib.org/index.html): librería de visualización para Python. Es requerida por algunos modulos temporales.

Por otra parte, utilizaremos algunos paquetes de [**R**](https://cran.r-project.org/): 
- [rgrass7](https://cran.r-project.org/web/packages/rgrass7/index.html): necesaria para la comunicación entre GRASS y R.
- [caret](https://cran.r-project.org/web/packages/caret/index.html): funciones de entrenamiento y visualización de modelos de regresión y clasificación. Es requerida por [v.class.mlR]()
- [sf](https://cran.r-project.org/web/packages/sf/index.html): soporte para datos vectoriales en formato *simple features*.
- [dplyr](https://cran.r-project.org/web/packages/dplyr/index.html): librería para manipulación de data.frames.
- entre otras.

Ver la [**Guía de instalación**](https://gitlab.com/veroandreo/maie-procesamiento/-/tree/master/pdf/00_instalacion.pdf)
para más detalles.

### Extensiones de GRASS GIS que utilizaremos durante el curso

* [i.segment.uspo](https://grass.osgeo.org/grass-stable/manuals/addons/i.segment.uspo.html): Optimización no supervisada de los parámetros de la segmentación
* [r.neighborhoodmatrix](https://grass.osgeo.org/grass-stable/manuals/addons/r.neighborhoodmatrix.html): Identifica relaciones de adyacencia entre objetos
* [r.object.geometry](https://grass.osgeo.org/grass-stable/manuals/addons/r.object.geometry.html): Calcula parámetros de geometría de objetos raster
* [i.segment.stats](https://grass.osgeo.org/grass-stable/manuals/addons/i.segment.stats.html): Calcula estadísticas de áreas raster
* [i.wi](https://grass.osgeo.org/grass-stable/manuals/addons/i.wi.html): Calcula diferentes tipos de índices de agua
* [i.superpixels.slic](https://grass.osgeo.org/grass-stable/manuals/addons/i.superpixels.slic.html): Realiza la segmentación de una imagen utilizando el método de segmentación SLIC
* [v.class.mlR](https://grass.osgeo.org/grass-stable/manuals/addons/v.class.mlR.html): Clasificación supervisada de vectores usando el paquete *caret* de R 
* [i.modis](https://grass.osgeo.org/grass-stable/manuals/addons/i.modis.html): Conjunto de herramientas para descargar y procesar productos MODIS utilizando *pyModis*
* [v.strds.stats](https://grass.osgeo.org/grass-stable/manuals/addons/v.strds.stats.html): Estadísticas zonales de determinados conjuntos de datos raster espacio-temporales basados en un mapa vectorial de polígonos
* [r.hants](https://grass.osgeo.org/grass-stable/manuals/addons/r.hants.html): Aproxima una serie temporal periódica y crea una salida reconstruída
* [r.seasons](https://grass.osgeo.org/grass-stable/manuals/addons/r.seasons.html): Extrae las estaciones a partir de una serie de tiempo
* [r.regression.series](https://grass.osgeo.org/grass-stable/manuals/addons/r.regression.series.html): Calcula los parámetros de regresión lineal entre dos series temporales
* [i.landsat](https://grass.osgeo.org/grass-stable/manuals/addons/i.landsat.html): Conjunto de herramientas para descargar e importar imágenes Landsat.
* [i.sentinel](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.html): Herramientas para descargar y procesar productos Copernicus Sentinel
* [i.fusion.hpf](https://grass.osgeo.org/grass-stable/manuals/addons/i.fusion.hpf.html): Fusión de datos pancromáticos de alta resolución y multiespectrales de baja resolución basados en la técnica de adición de filtros de paso alto
* [r.learn.ml](https://grass.osgeo.org/grass-stable/manuals/addons/r.learn.ml.html): Clasificación supervisada y regresión de mapas raster usando el paquete *scikit-learn* de python

Las instalaremos durante el curso con: `g.extension extension=nombredelaextension`

## Datos

* Ejercicios 1 y 2:
  * [Location North Carolina (150Mb)](https://grass.osgeo.org/sampledata/north_carolina/nc_spm_08_grass7.zip): descomprimir en `$HOME/grassdata`
  * [Mapas raster (1Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/sample_rasters.zip?inline=false): descargar y mover a `$HOME/gisdata`
  * [Mapa vectorial (3Mb)](https://gitlab.com/veroandreo/grass-gis-conae-es/raw/master/data/streets.gpkg?inline=false): descargar y mover a `$HOME/gisdata`
* Presentación y ejercicio series de tiempo + ejercicio OBIA:
  * [Location Córdoba (150Mb)](https://drive.google.com/file/d/1uTwTy34vtqqxW_8sjWKF4zSpcXHkBFko/view?usp=sharing): descomprimir en `$HOME/grassdata`
  * [Datos SPOT](https://aulavirtual.ig.conae.unc.edu.ar/): Estos datos estarán en el aula virtual por tener licencia especial
* Presentaciones y ejercicios opcionales:
  * Escenas [Landsat 8 14/01/2020 (979Mb)](https://drive.google.com/file/d/1ytQp-xin1FQr_hqtDJRLgK6g4eXwK-WI/view?usp=sharing) y [Landsat 8 02/03/2020 (880Mb)](https://drive.google.com/file/d/1Gg8FbhwpIQR-GyYepM4uw_9IOjEnji_N/view?usp=sharing): descargar y mover a `$HOME/gisdata/landsat_data`. **NO DESCOMPRIMIR**
  * Escena [Sentinel 2 (720Mb)](https://drive.google.com/file/d/1RbTMjvElQX_EOwE07GXYOq4rJohCOTvq/view?usp=sharing): descargar y mover a `$HOME/gisdata/s2_data`. **NO DESCOMPRIMIR**

## Sobre la disertante

**Verónica Andreo** es investigadora asistente de [CONICET](http://www.conicet.gov.ar/?lan=en)
y desarrolla sus actividades en el Instituto de Altos Estudios Espaciales "Mario Gulich" 
[(IG)](https://ig.conae.unc.edu.ar/) dependiente de la CONAE y la UNC. Su 
investigación esta enfocada en las aplicaciones del sensado remoto y los sistemas de 
información geográfica (SIG) a problemas relacionados con la Salud Pública y los huéspedes
y vectores de enfermedades zoonóticas.  
Vero es Charter member de [OSGeo](http://www.osgeo.org/) y promotora de 
[FOSS4G](http://foss4g.org/). Además, es parte del 
[GRASS GIS Development team](https://grass.osgeo.org/about/community), chair del 
proyecto desde Febrero 2021 (primera chair mujer y latina) 
y, dicta cursos y talleres introductorios y avanzados sobre los módulos temporales de 
GRASS GIS y sus aplicaciones. Para más detalles, visitar: https://veroandreo.gitlab.io/.

## Agradecimientos

- A todos los desarrolladores, usuarios y entusiastas del software libre y de código abierto (FOSS)
- A la [NASA](https://www.nasa.gov/) y la [ESA](http://www.esa.int/) por poner a disposición del público los datos y productos satelitales
- Al [IGN](https://www.ign.gob.ar/) e [IDECOR](https://idecor.cba.gov.ar/) por las capas vectoriales y de cobertura de acceso libre y gratuito

## Licencias

Las presentaciones fueron creadas con [gitpitch](https://gitpitch.com/) bajo la licencia **MIT**. 
Todo el material del curso en este repositorio está bajo licencia Creative Commons Attribution-ShareAlike 4.0 
International.

[![Creative Commons](assets/img/ccbysa.png)](http://creativecommons.org/licenses/by-sa/4.0/) 
