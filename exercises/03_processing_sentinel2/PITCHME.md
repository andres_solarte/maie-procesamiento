---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Ejercicio: Trabajamos con imágenes Sentinel 2 
<br>
@fa[satellite text-gray fa-3x]

---

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Búsqueda y descarga de escenas Sentinel 2
- Importar datos Sentinel 2 a GRASS GIS
- Calibración y corrección atmosférica (Nivel 1C)
- Balance de colores y composición RGB
- Detección de nubes y sombras
- Índices de agua y vegetación
- Segmentación
- Classificación supervisada 
@olend
@snapend

---

@snap[north span-100]
### Datos Sentinel 2
@snapend

@snap[west span-40]
<br>
![Sentinel 2 satellite](assets/img/sentinel2.jpg)
@snapend

@snap[east span-60]
<br><br>
@ul[list-content-verbose](false)
- Lanzamiento: Sentinel-2A en 2015, Sentinel-2B en 2017
- Tiempo de revisita: ~5 días
- Cobertura sistemática de áreas terrestres y costeras entre los 84°N y 56°S
- 13 bandas espectrales con resolución espacial de 10 m (VIS y NIR), 20 m (red-edge y SWIR) y 60 m (otras)
@ulend
@snapend

+++

![Sentinels](assets/img/sentinel_satellites.jpg)

@size[22px](ESA - Satélites Copernicus Sentinel. Más información en: <br>https://www.copernicus.eu/en/about-copernicus/infrastructure/discover-our-satellites)

+++

![Sentinel and Landsat bands](assets/img/landsat_and_sentinel_bands.png)

@size[28px](Distribución de bandas de Sentinel 2 comparadas con Landsat)

---

### @fa[plug text-green] Extensiones para datos Sentinel @fa[plug text-green]

- [i.sentinel.download](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.download.html): descarga productos Copernicus Sentinel de Copernicus Open Access Hub
- [i.sentinel.import](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.import.html): importa datos Sentinel descargados de Copernicus Open Access Hub
- [i.sentinel.preproc](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.preproc.html): importa y realiza corrección atmosférica y topográfica de imágenes S2
- [i.sentinel.mask](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.mask.html): crea máscaras de nubes y sombras para imágenes S2

@size[24px](Ver <a href="https://grasswiki.osgeo.org/wiki/SENTINEL">Sentinel wiki</a> para más detalles)

+++

Recientemente, se sumaron dos nuevos miembros en la familia *i.sentinel*:

- [i.sentinel.coverage](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.coverage.html): comprueba la cobertura de área de las escenas de S1 o S2 seleccionadas
- [i.sentinel.parallel.download](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.parallel.download.html): descarga imagenes Sentinel en paralelo

+++

- Para conectarse al [Copernicus Open Access Hub](https://scihub.copernicus.eu/) a través de [i.sentinel.download](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.download.html), se necesita ser usuario [registrado](https://scihub.copernicus.eu/dhus/#/self-registration)
- Crear el archivo *`SENTINEL_SETTING.txt`* en el directorio *`$HOME/gisdata/`* con el siguiente contenido:

```
your_username
your_password
```

+++

@fa[download fa-2x text-green] Descargar el [código](https://gitlab.com/veroandreo/maie-procesamiento/raw/master/code/04_S2_cba_imagery_code.sh?inline=false) para seguir esta sesión

---

### Niveles de procesamiento Sentinel 2

- **L1C**: Reflectancia a tope de atmósfera o Top of Atmosphere (TOA). Disponibles desde el lanzamiento.
- **L2A**: Reflectancia Superficial o Bottom of Atmosphere (BOA), i.e., los datos han sido corregidos para remover los efectos de la atmósfera. Sólo desde 2019.

+++

### Archivo de datos Sentinel 

> @fa[archive text-green] **Long Term Archive (LTA)** @fa[archive text-green]
>
> Todos los productos (1C o 2A) de más de un año son movidos fuera de línea 
> y se requiere un tiempo de espera para ponerlos a disposición del usuario. 
> Esto dificulta la automatización de tareas con productos de más de 12 
> meses de antigüedad. 

@fa[meh-rolling-eyes text-orange fa-2x]

---

Iniciar GRASS GIS, crear nuevo mapset y establecer región computacional

@code[bash](code/04_S2_cba_imagery_code.sh)

@[11-17](Iniciar GRASS GIS en el location *posgar2007_4_cba/PERMANENT*)
@[19-20](Crear un nuevo mapset *sentinel2*)      
@[22-24](Definir la región computacionalal radio urbano de Córdoba)      

---

### Búsqueda y descarga de datos S2

@code[bash](code/04_S2_cba_imagery_code.sh)

@[27-33](Instalar la extensión *i.sentinel*)
@[35-41](Lista de escenas disponibles que *intersectan* la región computacional)
@[43-50](Lista de escenas disponibles que *contienen* la región computacional)
@[52-57](Descargar la escena seleccionada - NO EJECUTAR)

+++

Como la descarga desde el Copernicus Open Access Hub toma su tiempo...
<br><br>

@fa[download fa-2x text-green] Descargar la escena [Sentinel 2](https://drive.google.com/file/d/1RbTMjvElQX_EOwE07GXYOq4rJohCOTvq/view?usp=sharing) que usaremos y moverla a *`HOME/gisdata/s2_data`*
<br>

+++

Hagamos una prueba con datos del LTA...

```bash
i.sentinel.download -l \
  settings=$HOME/gisdata/SENTINEL_SETTING.txt \
  start="2019-01-01" \
  end="2020-02-28" \
  clouds=30

i.sentinel.download \
  settings=$HOME/gisdata/SENTINEL_SETTING.txt \
  uuid=d4e5df0e-7ead-4407-ba82-d2583be1a6b8 \
  output=$HOME/gisdata/s2_data
```

+++

@fa[meh fa-2x text-orange]

<br>

![](assets/img/S2_LTA.png)

+++

### En desarrollo:

una version de `i.sentinel.download` que descarga datos de USGS Earth Explorer a través de landsatxplore como `i.landsat.download`

https://github.com/OSGeo/grass-addons/pull/419

@fa[grin-stars text-orange fa-2x]

---

### Importar datos Sentinel 2 a GRASS GIS
<br>
![](assets/img/sentinel_import_options.png)

+++

#### 1. Importar con corrección atmosférica: [i.sentinel.preproc](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.preproc.html)
##### Productos nivel 1C
<br>
![](assets/img/i_sentinel_preproc.png)

+++
@snap[north span-100]
Para obtener un valor de AOD, tenemos 2 opciones:
@snapend

@snap[west span-50 text-center text-08]
A. Estimar el valor desde un grafico
<br>
@img[90px](assets/img/S2_AOD_plot.png)
@snapend

@snap[east span-50 text-center text-08]
B. Descargar un archivo y el valor sera estimado 
<br>
<img src="assets/img/S2_AOD_file.png" width="30%">
@snapend

@snap[south span-100 text-07]
[http://aeronet.gsfc.nasa.gov](https://aeronet.gsfc.nasa.gov)
<br><br>
@snapend

+++

@snap[north-east span-100]
Obtener AOD de <br>
[http://aeronet.gsfc.nasa.gov](https://aeronet.gsfc.nasa.gov)
@snapend

@snap[west span-50]
<img src="assets/img/S2_AOD_file.png" width="65%">
@snapend

@snap[east span-60]
<br>
@ul[list-content-verbose](false)
- Estación *ARM_Cordoba* o *Pilar_Cordoba*
- Seleccionar fechas de inicio y final
- Seleccionar: *`Combined file`* y *`All points`*
- Descargar y descomprimir (el archivo final tiene extensión .dubovik)
- Pasar el archivo con la opción `aeronet_file`
@ulend
@snapend

+++

@fa[mountain text-green] Mapa de elevación @fa[mountain text-green] 
<br>

- [r.in.srtm.region](https://grass.osgeo.org/grass-stable/manuals/addons/r.in.srtm.region.html): importa (y re-proyecta) los mosaicos SRTM que cubren la región computacional, parchea los mosaicos e interpola datos faltantes
- [r.in.nasadem](https://grass.osgeo.org/grass-stable/manuals/addons/r.in.nasadem.html): importa (y re-proyecta) los mosaicos de NASADEM que cubren la región computacional y parchea los mosaicos

> @fa[exclamation-triangle text-orange] Si el DEM es más chico que la 
> región computacional, sólo la región cubierta por el DEM será corregida 
> atmosféricamente...

+++

Ejemplo

```bash
# enter directory with Sentinel scene and unzip file
cd $HOME/gisdata/s2_data/
unzip $HOME/gisdata/s2_data/name_of_S2_scene

i.sentinel.preproc -atr \
  input_dir=$HOME/gisdata/s2_data/name_of_S2_scene.SAFE \
  elevation=NASADEM \
  aeronet_file=$HOME/gisdata/s2_data/name_of_aeronet_station.dubovik
```

---

#### 2. Importar sin corrección atmosférica (as is): [i.sentinel.import](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.import.html)
##### Productos nivel 2A

@code[bash](code/04_S2_cba_imagery_code.sh)

@[59-61](Imprimir información sobre las bandas antes de importarlas)
@[63-67](Importar bandas seleccionadas, recortar y reproyectar al vuelo)
@[69-74](Listar bandas importadas y revisar metadatos)

---

#### Balance de colores y composiciones

@code[bash](code/04_S2_cba_imagery_code.sh)

@[77-84](Asignar *grey* como paleta de colores)
@[86-91](Ajuste de colores para una composición RGB color natural)
@[93-98](Mostrar la combinación RGB - *d.rgb*)

+++

@img[span-80](assets/img/S2_color_enhance_corr_full.png)

@size[24px](Escena Sentinel 2 con colores balanceados - Composición RGB 432)

+++

> @fa[tasks] **Tarea** 
>
> Realizar balance de colores y mostrar combinacion falso color NIR-RED-GREEN


---

#### Máscara de nubes y sombras de nubes

@code[bash](code/04_S2_cba_imagery_code.sh)

@[106-118](Identificar y enmascarar nubes y sus sombras)
@[120-127](Visualización de la salida)

+++

<img src="assets/img/S2_clouds_and_shadows.png" width="55%" alt="Clouds and cloud shadows">

@size[24px](Nubes y sombras de nubes identificadas con [i.sentinel.mask](https://grass.osgeo.org/grass-stable/manuals/addons/i.sentinel.mask.html))

---

### Índices de agua y vegetación

@code[bash](code/04_S2_cba_imagery_code.sh)

@[130-136](Definir región computacional)
@[138-141](Establecer máscara)
@[143-148](Estimación de los índices de vegetación)
@[150-151](Instalar extensión *i.wi*)
@[153-160](Estimación de índice de agua)

+++

<img src="assets/img/S2_NDVI.png" width="48%">
<img src="assets/img/S2_NDWI.png" width="48%"> 

@size[24px](NDVI y NDWI de Sentinel 2)

---

### Segmentación

@code[bash](code/04_S2_cba_imagery_code.sh)

@[163-169](Instalar la extensión *i.superpixels.slic*)
@[171-176](Listar los mapas y crear grupos y subgrupos)
@[178-181](Ejecutar *i.superpixels.slic*)
@[183-185](Convertir el resultado a vector)
@[187-189](Ejecutar *i.segment*)
@[191-193](Convertir el resultado a vector)
@[195-199](Mostrar NDVI junto con las 2 salidas de la segmentación)

+++

<img src="assets/img/S2_superpixels.png" width="48%">
<img src="assets/img/S2_segments.png" width="48%">

@size[24px](Resultados de la segmentación: <br> A. Con superpixels a la izquierda, B. Con region growing a la derecha)

+++

> @fa[tasks] **Tarea** 
>
> Ejecutar cualquiera de los 2 métodos de segmentación con diferentes parámetros y comparar los resultados

---

@snap[north span-100]
### Clasificación supervisada
@snapend

@snap[west span-40]
<br>
@fa[tasks] **Tarea** 
@ol[](false)
- digitalizar áreas de entrenamiento para 3 clases con [g.gui.iclass](http://grass.osgeo.org/grass-stable/manuals/g.gui.iclass.html)
- guardarlas en un mapa vectorial: `training`
@olend
@snapend

@snap[east span-60]
<br>
![g.gui.iclass](assets/img/g_gui_iclass.png)
@snapend

+++

Clasificación supervisada con Maximum Likelihood

@code[bash](code/04_S2_cba_imagery_code.sh)

@[202-209](Convertir el vector de áreas de entrenamiento a raster)
@[211-214](Generar archivos de firma espectral)
@[216-219](Realizar la clasificación por Maximum Likelihood)
@[221-226](Añadir etiquetas a las clases)

+++

@img[span-85](assets/img/sentinel_maxlik.png)

@size[24px](Clasificación supervisada con Maximum Likelihood)

+++

Clasificación supervisada con Machine Learning

@code[bash](code/04_S2_cba_imagery_code.sh)

@[229-235](Instalar la extensión *r.learn.ml*)
@[237-239](Realizar la clasificación por RF)
@[241-246](Añadir etiquetas a las clases)

+++

@img[span-85](assets/img/sentinel_rf.png)

@size[24px](Clasificación supervisada con Random Forest)

+++

> @fa[tasks] **Tarea** 
>
> Comparar los resultados de ambos tipos de clasificación supervisada a través del índice Kappa

<br><br>
@snap[south-east span-70]
@img[width=90px](assets/img/tip.png) Hay un módulo [r.kappa](https://grass.osgeo.org/grass-stable/manuals/r.kappa.html)
<br><br>
@snapend

+++

### Post-procesamiento y validación

- [r.reclass.area](http://grass.osgeo.org/grass-stable/manuals/r.reclass.area.html) para eliminar pequeñas áreas, enmascarar nuevos valores y rellenar los huecos con [r.neighbors](http://grass.osgeo.org/grass-stable/manuals/r.neighbors.html) o [r.fillnulls](http://grass.osgeo.org/grass-stable/manuals/r.fillnulls.html)
- convertir la salida en vector y ejecutar [v.clean](http://grass.osgeo.org/grass-stable/manuals/v.clean.html) con `tool=rmarea`
- [r.kappa](https://grass.osgeo.org/grass-stable/manuals/r.kappa.html) para la validación (idealmente también digitalizar una muestra de prueba)

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Object based image analysis - OBIA](https://gitlab.com/veroandreo/maie-procesamiento/-/blob/master/pdf/05_obia.pdf)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
